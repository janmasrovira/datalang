module Main (main) where

import DataLangUtils
import System.Environment
import Options.Applicative
import Options.Applicative.Help.Pretty

data Options = Options {
  _buildDir :: FilePath
  , _name :: String
  , _orgFile :: FilePath
  }

parseOptions :: Parser Options
parseOptions = do
  _buildDir <- strOption
    (long "build-dir"
     <> short 'd'
     <> metavar "DIR"
     <> showDefault
     <> value "build"
     <> help "Target output directory")
  _name <- strOption
    (long "name"
     <> short 'n'
     <> metavar "STRING"
     <> showDefault
     <> value "spec"
     <> help "Base name of the output files")
  _orgFile <- argument str
     (metavar "ORG_FILE"
     <> help "Path to an org file"
     )
  pure Options {..}

descr :: ParserInfo Options
descr = info (parseOptions <**> helper)
       (fullDesc
        <> progDesc "Parse an org mode file with a datalang specification and compile it to all available formats."
        <> headerDoc (Just $ dullblue $ bold $ underline "datalang help")
        <> footerDoc (Just foot)
       )
  where
  foot :: Doc
  foot = bold "maintainer: " <> "janmasrovira@gmail.com (Formal Vindications)"

go :: Options -> IO ()
go Options{..} = exportAll _buildDir _name _orgFile

main :: IO ()
main = execParser descr >>= go
