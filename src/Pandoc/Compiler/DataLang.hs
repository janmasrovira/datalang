module Pandoc.Compiler.DataLang (compile) where

import           Common                hiding (Space)
import           Data.Data
import qualified Data.HashMap.Strict   as HashMap
import qualified Data.Text             as Text
import qualified Data.Text.IO          as T
import qualified DataLang.Parser       as D
import qualified DataLang.Syntax.Extra as D
import           Pandoc.Extra
import           Text.Megaparsec       (MonadParsec, Parsec)
import qualified Text.Megaparsec       as M

import           Text.Pandoc.Writers


type CodeBlockText = Text

-- | Concatenates all @datalang@ code blocks which finds in the document and
-- concatenates them. It returns the original document without the extracted
-- code blocks.
extractDataLang :: Pandoc -> (Pandoc, CodeBlockText)
extractDataLang p = (walk rmDataLangBlock p, query catDataLangBlock p)
  where
  datalangStr = "datalang"

  catDataLangBlock :: Block -> Text
  catDataLangBlock (CodeBlock (_, [lang], _) t)
    | datalangStr == lang = t
  catDataLangBlock _ = mempty

  rmDataLangBlock (CodeBlock (_, [lang], _) t)
    | datalangStr == lang = Null
  rmDataLangBlock a = a

-- | Extracts the documentation of all identifiers.
extractIdenDocs :: PandocForest -> (PandocForest, HashMap Text PandocForest)
extractIdenDocs pf = (topDown rmIdenDoc pf , queryWith getDoc pf)
  where
    rmIdenDoc :: [(HeaderNode, PandocForest)] -> [(HeaderNode, PandocForest)]
    rmIdenDoc = filter (not . isIdenDoc . fst)
     where
       isIdenDoc :: HeaderNode -> Bool
       isIdenDoc = isJust . attrDocFor . _attrs
    attrDocFor :: Attr -> Maybe Text
    attrDocFor (_, _, as) = lookup "docfor" $ map (first (Text.map toLower)) as
    getDoc :: (HeaderNode , PandocForest) -> HashMap Text PandocForest
    getDoc (HeaderNode lvl attr _, pf)
     | (Just iden) <- attrDocFor attr = HashMap.singleton iden
        (flushLeft (lvl - 1) $ topDown rmIdenDoc pf)
    getDoc _ = mempty
    flushLeft :: Int -> PandocForest -> PandocForest
    flushLeft i = bottomUp flush
      where
      flush :: HeaderNode -> HeaderNode
      flush (HeaderNode lvl a inl) =  HeaderNode (lvl - i) a inl

compile :: Pandoc -> Either Text D.ModuleDef
compile pan = do
  let (Pandoc m bs, datalangText) = extractDataLang pan
  pf <- maybeToEither "parsePandocForest" $ parsePandocForest bs
  let (doc, hmap) = extractIdenDocs pf
  D.ModuleDef {..} <- annotateIdens hmap <$> D.parseModule datalangText
  return D.ModuleDef {
    _documentation = Pandoc m (flattenForest doc)
    , _meta
    , _typeDefs
    , _functionDefs
  }
  where
  annotateIdens :: HashMap Text PandocForest -> D.ModuleDef -> D.ModuleDef
  annotateIdens hm m = foldr step m (HashMap.toList hm)
    where
    flatten = flattenForest
    step :: (Text, PandocForest) -> D.ModuleDef -> D.ModuleDef
    step (t, f) m = case Text.splitOn "." t of
      [ty]
        | D.typeDefExists ty m -> D.documentTypeDefUnsafe ty (flatten f) m
        | D.functionExists ty m -> D.documentFunctionUnsafe ty (flatten f) m
        | otherwise -> error ("no function nor type named " <> unpack ty)
      [ty, x]
        | isUpper (Text.head x) -> D.documentConstructor ty x (flatten f) m
        | D.fieldExists ty x m -> D.documentFieldUnsafe ty x (flatten f) m
        | D.argExists ty x m -> D.documentArgUnsafe ty x (flatten f) m
      [ty, constr, field] -> D.documentConstructorField ty constr field (flatten f) m
      _ -> error ("unrecognised tag: " <> unpack t)
