module Pandoc.Lens where

import           Control.Lens
import           Text.Pandoc

body :: Lens' Pandoc [Block]
body = lens (\(Pandoc _ b)->b) (\(Pandoc m _) b -> Pandoc m b)
