module Pandoc.Extra (
  module Text.Pandoc
  , module Pandoc.Extra
  , module Text.Pandoc.Walk
                    ) where

import           Common                    hiding (Space)
import qualified Control.Monad.Combinators as C
import qualified Data.Text                 as Text
import           Text.Megaparsec           (MonadParsec, Parsec)
import qualified Text.Megaparsec           as M
import           Text.Pandoc
import           Text.Pandoc.Walk

type ParserBlock = Parsec Text [Block]

data HeaderNode = HeaderNode {
  _level     :: Int
  , _attrs   :: Attr
  , _inlines :: [Inline]
  }
  deriving (Show, Data)
makeLenses ''HeaderNode

-- | All 'Block's in the '_preface' are guaranteed __not__ to be a 'Header'.
data PandocForest = PandocForest {
  _preface    :: [Block]
  , _sections :: [(HeaderNode, PandocForest)]
  }
  deriving (Show, Data)
makeLenses ''PandocForest

isHeader :: Block -> Bool
isHeader Header{} = True
isHeader _        = False

headedForest :: Int -> ParserBlock (HeaderNode, PandocForest)
headedForest minlvl = do
  header@(HeaderNode lvl _ _) <- M.token testHeader mempty
  forest <- panForest lvl
  return (header, forest)
  where
  testHeader :: Block -> Maybe HeaderNode
  testHeader h@(Header l as t)
    | l > minlvl = Just (HeaderNode l as t)
  testHeader _ = Nothing

panForest :: Int -> ParserBlock PandocForest
panForest curLvl = do
  _preface <- M.takeWhileP (Just "Block") (not . isHeader)
  _sections <- many (headedForest curLvl)
  return PandocForest{..}

parsePandocForest :: [Block] -> Maybe PandocForest
parsePandocForest = M.parseMaybe (panForest 0 <* M.eof)

parsePandocForest' :: [Block] -> PandocForest
parsePandocForest' =
  fromMaybe (error "parsePandocForest'")
  . M.parseMaybe (panForest 0 <* M.eof)

flattenForest :: PandocForest -> [Block]
flattenForest = go
  where
  go :: PandocForest -> [Block]
  go PandocForest {..} = _preface <> concatMap goH _sections
  goH :: (HeaderNode, PandocForest) -> [Block]
  goH (HeaderNode{..}, f) = Header _level _attrs _inlines : go f

-- | for debugging.
printSections :: PandocForest -> Text
printSections = Text.unlines . map (printHeaded 1) . _sections
  where
    printHeaded :: Int -> (HeaderNode, PandocForest) -> Text
    printHeaded i (HeaderNode{..}, PandocForest{..}) =
      Text.replicate i "*" <> " " <> Text.pack (show _inlines) <> "\n"
      <>  mconcatMap (printHeaded (i + 1)) _sections

-- | Adapted from 'docTitle'.
docSubtitle :: Meta -> [Inline]
docSubtitle meta =
  case lookupMeta "subtitle" meta of
         Just (MetaString s)           -> [Str s]
         Just (MetaInlines ils)        -> ils
         Just (MetaBlocks [Plain ils]) -> ils
         Just (MetaBlocks [Para ils])  -> ils
         _                             -> []

-- | Uses softbreak as a delimiter for various Authors.
docAuthors' :: Meta -> [[Inline]]
docAuthors' = fromMaybe err . M.parseMaybe authors . concat . docAuthors
  where
  err = error "Unexpected error parsing authors"
  authors :: Parsec Text [Inline] [[Inline]]
  authors = C.sepBy author sepParser
  author :: Parsec Text [Inline] [Inline]
  author = M.takeWhileP (Just "Author Inline") (not . isSep)
  -- | Determines if an 'Inline' qualifies as a separator.
  isSep :: Inline -> Bool
  isSep SoftBreak = True
  isSep _         = False
  sepParser :: Parsec Text [Inline] ()
  sepParser = M.token sepP mempty
    where sepP b = if isSep b then Just () else Nothing

queryImagesRelPaths :: Pandoc -> [FilePath]
queryImagesRelPaths = filter isRelative . queryImagesPaths

queryImagesPaths :: Pandoc -> [FilePath]
queryImagesPaths = query getImagePath

getImagePath :: Inline -> [FilePath]
getImagePath i = case i of
   Image _ _ (path, _) -> [unpack path]
   _                   -> []
