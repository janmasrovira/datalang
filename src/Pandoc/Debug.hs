module Pandoc.Debug where

import           Common
import           Data.Text.IO    as T
import           DataLang.Syntax (ModuleDef)
import           Text.Pandoc

readPandoc :: FilePath -> IO (Either PandocError Pandoc)
readPandoc orgFile = runPure . readOrg def <$> T.readFile orgFile

readPandocShow :: FilePath -> IO Text
readPandocShow = fmap showT . readPandoc
