module Coq.Syntax where

import           Data.List.Extra
import           Data.Set        (Set)
import           Data.String
import           Data.Text       (Text)
import qualified Data.Text       as T

data Type =
  TyDataDef Text
  | TyNat
  | TyString
  | TyBool
  | TySet
  | TyType
  | TySeqOf Type
  | TyFun Type Type
 deriving (Show)

data FieldDef =
  FieldDef {
   _fieldName    :: Text
   ,  _fieldType :: Type
   }
 deriving (Show)

data DataDef = DataDef {
  _name   :: Text
  , _type :: Type
  , _rhs  :: DataDefRhs
  }
 deriving (Show)

data ConstructorDef =
  ConstructorDef {
  _constructorName   :: Text
  , _constructorType :: Type
  }
 deriving (Show)

data DataDefRhs =
  Record {
     _constructor :: Text
     , _fields :: [FieldDef]
     }
  | SumType {
      _constructors :: [ConstructorDef]
      }
  | Synonym {
      _synonymType :: Type
      }
 deriving (Show)

data Statement =
  TopDataDef DataDef
  | Verbatim Text
  deriving (Show)

type Iden = Text

newtype ModuleDef = ModuleDef {
  _statements :: [Statement]
  }
  deriving (Show)
