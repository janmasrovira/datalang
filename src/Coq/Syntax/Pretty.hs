module Coq.Syntax.Pretty where

import           Common
import           Coq.Syntax.Extra
import qualified Data.Set                  as Set
import           Prettyprinter
import           Prettyprinter.Render.Text

newtype Config = Config {
  _indent :: Int
  }

config :: Config
config = Config {
  _indent = 2
  }

prettyFieldDef :: FieldDef -> Doc a
prettyFieldDef FieldDef {..} =
  pretty _fieldName <+> colon <+> prettyType _fieldType

prettyType :: Type -> Doc a
prettyType t = case t of
  TySeqOf l   -> "seq" <+> prettyType l
  TyNat       -> "nat"
  TyString    -> "string"
  TyBool      -> "bool"
  TySet       -> "Set"
  TyType      -> "Type"
  TyFun a b   -> parensType a <+> "->" <+> parensType b
  TyDataDef t -> pretty t

parensType :: Type -> Doc a
parensType t
  | typeNeedsParens t = parens (prettyType t)
  | otherwise = prettyType t

prettyConstructorDef :: ConstructorDef -> Doc a
prettyConstructorDef ConstructorDef{..} =
  pipe <+> pretty _constructorName <+> colon <+> prettyType _constructorType

hang' :: Doc a -> Doc a
hang' = hang (_indent config)

sepBy :: Foldable t => Doc ann -> t (Doc ann) -> Doc ann
sepBy p = concatWith (\a b -> a <> p <> b)

prettyDataDefRhs :: DataDefRhs -> Doc a
prettyDataDefRhs d = case d of
  SumType{..} -> line <> vsep (map prettyConstructorDef _constructors)
  Record{..}  -> space <> pretty _constructor <+> braces
        (line <> sepBy (semi <> line) (map prettyFieldDef _fields) <> line)
  Synonym{..} -> space <> prettyType _synonymType

colonEquals :: Doc a
colonEquals = colon <> equals

prettyDataDef :: DataDef -> Doc a
prettyDataDef DataDef{..} =
  hang' $ dataDefKw _rhs <+> pretty _name <+> colon <+> prettyType _type <+>
  colonEquals <> prettyDataDefRhs _rhs <> dot

dataDefKw :: DataDefRhs -> Doc a
dataDefKw d = case d of
    Record{}  -> "Record"
    SumType{} -> "Inductive"
    Synonym{} -> "Definition"

typeNeedsParens :: Type -> Bool
typeNeedsParens t =
  case t of
    TyDataDef{} -> False
    TyNat       -> False
    TyString    -> False
    TyBool      -> False
    TySet       -> False
    TyType      -> False
    TySeqOf{}   -> True
    TyFun{}     -> True

prettyStatement :: Statement -> Doc a
prettyStatement s =
  case s of
    TopDataDef d -> prettyDataDef d
    Verbatim txt -> pretty txt

prettyStatements :: [Statement] -> Doc a
prettyStatements = snd . foldl' aux (Nothing, mempty)
  where
   aux :: (Maybe Statement, Doc a) -> Statement -> (Maybe Statement, Doc a)
   aux (Nothing, _) s = (Just s, prettyStatement s)
   aux (Just last, p) s = case (last, s) of
     (Verbatim{}, Verbatim{}) -> noLine
     (_, _)                    -> addLine
     where
       sep ln = (Just s, p <> ln <> prettyStatement s)
       addLine = sep (line <> line)
       noLine = sep line

prettyModuleDef :: ModuleDef -> Doc a
prettyModuleDef = prettyStatements . _statements

prettyModuleDefText :: ModuleDef -> Text
prettyModuleDefText = renderStrict . layoutPretty defaultLayoutOptions . prettyModuleDef
