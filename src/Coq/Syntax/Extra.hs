module Coq.Syntax.Extra (
  module Coq.Syntax.Extra
  , module Coq.Syntax)
 where

import Coq.Syntax
import Common

(-->) :: Type -> Type -> Type
(-->) = TyFun

constructorTypeFromArg :: Text -> Maybe Type -> Type
constructorTypeFromArg typeName arg =
  case arg of
    Nothing -> TyDataDef typeName
    Just ty -> ty --> TyDataDef typeName
