{-# LANGUAGE FunctionalDependencies     #-}
module JsonSchema.Builder where

import           Common
import           Data.Aeson
import           Data.Aeson.Encode.Pretty
import           Data.ByteString.Lazy      (toStrict)
import qualified Data.HashMap.Strict       as H
import           DataLang.Syntax.Extra     as D
import           JsonSchema.Syntax         as Schema

newtype SchemaError =
 DuplicateField Text
 deriving (Show)

class IsSchemaError e where
  fromBuildError :: SchemaError -> e

instance IsSchemaError Text where
  fromBuildError = pack . show

throwBuildError :: (IsSchemaError e, MonadError e m) => SchemaError -> m a
throwBuildError = throwError . fromBuildError

newtype SchemaBuilder = SchemaBuilder {
   _nodes :: Schema
  }
  deriving (Show)

addNode :: (IsSchemaError e, MonadError e m) =>
  SchemaNode -> SchemaBuilder -> m SchemaBuilder
addNode node (SchemaBuilder s@(Schema l)) =
  case H.lookup (key node) l of
    Nothing -> return (SchemaBuilder (insertNode node s))
    Just (AllOf as)
      | (AllOf as') <- node -> return (SchemaBuilder (insertNode (AllOf $ as <> as') s))
      | _ <- node -> error "impossible"
    Just _ -> throwBuildError (DuplicateField (key node))

emit :: (IsSchemaError e, MonadError e m, MonadState SchemaBuilder m) => SchemaNode -> m ()
emit node = get >>= addNode node >>= put

annotation :: (IsSchemaError e, MonadError e m, MonadState SchemaBuilder m)
  => Annotation -> m ()
annotation = mapM_ emit . annotationToNodes

withBlock :: (IsSchemaError e, MonadError e m, MonadState SchemaBuilder m) => m a -> m Schema
withBlock p = do
  b <- get
  put (SchemaBuilder mempty)
  p
  res <- gets _nodes
  put b
  return res

anyOf :: (IsSchemaError e, MonadError e m, MonadState SchemaBuilder m)
  => [m ()] -> m ()
anyOf = mapM withBlock >=> emit . AnyOf

allOf :: (IsSchemaError e, MonadError e m, MonadState SchemaBuilder m)
  => [m ()] -> m ()
allOf = mapM withBlock >=> emit . AllOf

allOfMaybe :: (IsSchemaError e, MonadError e m, MonadState SchemaBuilder m)
  => [m ()] -> m ()
allOfMaybe [] = return ()
allOfMaybe l  = allOf l

not_ :: (IsSchemaError e, MonadError e m, MonadState SchemaBuilder m)
  => m () -> m ()
not_ = withBlock >=> emit . Not

const_ :: (IsSchemaError e, MonadError e m, MonadState SchemaBuilder m)
  => Value -> m ()
const_ = emit . Const

items :: (IsSchemaError e, MonadError e m, MonadState SchemaBuilder m)
  => m () -> m ()
items = withBlock >=> emit . Items

itemsTuple :: (IsSchemaError e, MonadError e m, MonadState SchemaBuilder m)
  => [m ()] -> m ()
itemsTuple = mapM withBlock >=> emit . ItemsTuple

itemsPair :: (IsSchemaError e, MonadError e m, MonadState SchemaBuilder m)
  => m () -> m () -> m ()
itemsPair a b = itemsTuple [a, b]

itemsFirst :: (IsSchemaError e, MonadError e m, MonadState SchemaBuilder m)
  => m () -> m ()
itemsFirst = itemsTuple . pure

itemsSecond :: (IsSchemaError e, MonadError e m, MonadState SchemaBuilder m)
  => m () -> m ()
itemsSecond = itemsPair (return ())

minItems :: (IsSchemaError e, MonadError e m, MonadState SchemaBuilder m)
  => Int -> m ()
minItems = emit . MinItems

maxItems :: (IsSchemaError e, MonadError e m, MonadState SchemaBuilder m)
  => Int -> m ()
maxItems = emit . MaxItems

exactItems :: (IsSchemaError e, MonadError e m, MonadState SchemaBuilder m)
  => Int -> m ()
exactItems n = minItems n >> maxItems n

ifThen :: (IsSchemaError e, MonadError e m, MonadState SchemaBuilder m)
  => m () -> m () -> m ()
ifThen t1 t2 = do
  withBlock t1 >>= emit . If
  withBlock t2 >>= emit . Then

schema :: (IsSchemaError e, MonadError e m, MonadState SchemaBuilder m)
  => Text -> m ()
schema = emit . DSchema

id_ :: (IsSchemaError e, MonadError e m, MonadState SchemaBuilder m)
  => Text -> m ()
id_ = emit . DId

properties :: (IsSchemaError e, MonadError e m, MonadState SchemaBuilder m)
  => [(Text, m ())] -> m ()
properties ps = do
  let (names, ms) = unzip ps
  ss <- mapM withBlock ms
  emit $ Properties (zip names ss)

property :: (IsSchemaError e, MonadError e m, MonadState SchemaBuilder m)
  => (Text, m ()) -> m ()
property = properties . pure

required :: (IsSchemaError e, MonadError e m, MonadState SchemaBuilder m)
  => [Text] -> m ()
required = emit . Required

additionalProperties :: (IsSchemaError e, MonadError e m, MonadState SchemaBuilder m)
  => Bool -> m ()
additionalProperties = emit . AdditionalProperties

type_ :: (IsSchemaError e, MonadError e m, MonadState SchemaBuilder m)
  => JsonType -> m ()
type_ = emit . Type

enum :: (IsSchemaError e, MonadError e m, MonadState SchemaBuilder m)
  => [Value] -> m ()
enum = emit . Enum

pattern_ :: (IsSchemaError e, MonadError e m, MonadState SchemaBuilder m)
  => Pattern -> m ()
pattern_ = emit . Pattern

patternText :: (IsSchemaError e, MonadError e m, MonadState SchemaBuilder m)
  => Text -> m ()
patternText = emit . Pattern . PString

enumText :: (IsSchemaError e, MonadError e m, MonadState SchemaBuilder m)
  => [Text] -> m ()
enumText = emit . Enum . map String

ref :: (IsSchemaError e, MonadError e m, MonadState SchemaBuilder m)
  => Text -> m ()
ref = emit . Ref

defsMaybe :: (IsSchemaError e, MonadError e m, MonadState SchemaBuilder m)
  => [(Text, m ())] -> m ()
defsMaybe [] = return ()
defsMaybe l  = defs l

defs :: (IsSchemaError e, MonadError e m, MonadState SchemaBuilder m)
  => [(Text, m ())] -> m ()
defs ps = do
  let (names, ms) = unzip ps
  ss <- mapM withBlock ms
  emit $ Defs (zip names ss)

buildSchema' :: IsSchemaError e => StateT SchemaBuilder (Except e) ()
   -> Either e Schema
buildSchema' = buildSchema . withBlock

buildSchema :: IsSchemaError e => StateT SchemaBuilder (Except e) Schema
   -> Either e Schema
buildSchema = runIdentity . buildSchemaT

buildSchemaT :: (Monad m, IsSchemaError e) =>
    StateT SchemaBuilder (ExceptT e m) Schema
   -> m (Either e Schema)
buildSchemaT build =
 runExceptT $ evalStateT build (SchemaBuilder mempty)
