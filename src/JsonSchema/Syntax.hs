{-# LANGUAGE CPP #-}
module JsonSchema.Syntax where

import           Common                     hiding (intercalate)
import           Control.Lens.TH
import           Data.Aeson
import qualified Data.HashMap.Strict        as H
import qualified Data.Text                  as Text
import qualified Data.Vector                as Vector
import           Language.Haskell.TH        (Exp, Q)
import           Language.Haskell.TH.Syntax (Lift)
import qualified Language.Haskell.TH.Syntax as TH
import           Lift.Aeson

data JsonType =
  TyString
  | TyInteger
  | TyNumber
  | TyBoolean
  | TyObject
  | TyArray
  | TyRef Text
  deriving (Show, Lift, Data)
makePrisms ''JsonType

data Pattern =
  PString Text
  | POr [Pattern]
  deriving (Eq, Ord, Show, Lift, Data)
makePrisms ''Pattern

data SchemaNode =
  AnyOf [Schema]
  | AllOf [Schema]
  | Not Schema
  | Const Value
  | Items Schema
  | ItemsTuple [Schema]
  | If Schema
  | Then Schema
  | Else Schema
  | DSchema Text
  | DId Text
  | Properties [(Text, Schema)]
  | Pattern Pattern
  | Required [Text]
  | Type JsonType
  | Enum [Value]
  | Examples [Value]
  | Description Text
  | AdditionalProperties Bool
  | Ref Text
  | MinItems Int
  | MaxItems Int
  | Defs [(Text, Schema)]
  | RawNode Text Value
  deriving (Show, Lift, Data)

instance Lift Schema where
   lift Schema{..} = [| Schema (H.fromList . map (first pack) $ o') |]
      where o' = map (first Text.unpack) . H.toList $ _schemaNodes
#if MIN_VERSION_template_haskell(2,17,0)
   liftTyped = TH.unsafeCodeCoerce . TH.lift
#elif MIN_VERSION_template_haskell(2,16,0)
   liftTyped = TH.unsafeTExpCoerce . TH.lift
#endif

newtype Schema =
  Schema {
    _schemaNodes      :: HashMap Text SchemaNode
  }
  deriving (Show, Semigroup, Monoid, Data)
makeLenses ''Schema
makePrisms ''SchemaNode

instance ToJSON JsonType where
  toJSON :: JsonType -> Value
  toJSON t = case t of
    TyNumber  -> String "number"
    TyString  -> String "string"
    TyInteger -> String "integer"
    TyObject  -> String "object"
    TyArray   -> String "array"
    TyBoolean -> String "boolean"
    TyRef r   -> String ("#/$defs/" <> r)

renderPattern :: Pattern -> Text
renderPattern p = case p of
  PString s -> s
  POr s     -> Text.intercalate " | " (map renderPattern s)

unwrapNode :: SchemaNode -> (Text, Value)
unwrapNode n = case n of
  AnyOf ss -> ("anyOf", Array (Vector.fromList $ map toJSON ss))
  AllOf ss -> ("allOf", Array (Vector.fromList $ map toJSON ss))
  Not s -> ("not", toJSON s)
  Const v -> ("const", v)
  Items i -> ("items", toJSON i)
  ItemsTuple l -> ("items", Array (Vector.fromList $ map toJSON l))
  If cond -> ("if", toJSON cond)
  Then the -> ("then", toJSON the)
  Else els -> ("else", toJSON els)
  DSchema s -> ("$schema", toJSON s)
  DId s -> ("$id", toJSON s)
  Properties s -> ("properties", Object (H.fromList (map (second toJSON) s)))
  Required s -> ("required", toJSON s)
  Type s -> ("type", toJSON s)
  Pattern p -> ("pattern", String (renderPattern p))
  Enum a -> ("enum", toJSON a)
  Examples a -> ("examples", toJSON a)
  Description d -> ("description", String d)
  Ref d -> ("$ref", String d)
  MinItems d -> ("minItems", toJSON d)
  MaxItems d -> ("maxItems", toJSON d)
  AdditionalProperties d -> ("additionalProperties", toJSON d)
  Defs d -> ("$defs", Object (H.fromList (map (second toJSON) d)))
  RawNode k v -> (k, v)

key :: SchemaNode -> Text
key = fst . unwrapNode

nodeToValue :: SchemaNode -> Value
nodeToValue = snd . unwrapNode

instance ToJSON Schema where
  toJSON :: Schema -> Value
  toJSON (Schema nodes) = Object (H.map nodeToValue nodes)

uri2019 :: Text
uri2019 = "http://json-schema.org/draft/2019-09/schema#"
