module JsonSchema.Syntax.Pretty where

import           Common
import           Data.Aeson
import           Data.Aeson.Encode.Pretty
import qualified Data.Aeson.Encode.Pretty as A
import qualified Data.ByteString.Lazy     as B
import           JsonSchema.Syntax
import           Prettyprinter

-- | Trivial definition.
prettySchema :: Schema -> Doc a
prettySchema = pretty . prettyJson
