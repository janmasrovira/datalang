module JsonSchema.Syntax.Extra (
  module JsonSchema.Syntax.Extra
  , module JsonSchema.Syntax
  ) where

import JsonSchema.Syntax
import Data.List.Extra

por :: [Pattern] -> Pattern
por = POr . nubOrd 
