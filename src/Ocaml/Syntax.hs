module Ocaml.Syntax where

import           Data.List.Extra
import           Data.Set        (Set)
import           Data.String
import           Data.Text       (Text)
import qualified Data.Text       as T

data Type =
  Leaf LeafType
  | ListOf LeafType
 deriving (Show)

data LeafType =
  TyDataDef Text
  | TyInteger
  | TyBool
  | TyString
  | TyFloat
 deriving (Show)

data FieldDef =
  FieldDef {
   _fieldName    :: Text
   ,  _fieldType :: Type
   }
 deriving (Show)

data QualifiedIden = QIden {
    _qualif :: [Text]
    , _iden :: Text
    }
 deriving (Show)

instance IsString QualifiedIden where
  fromString :: String -> QualifiedIden
  fromString str = case unsnoc (T.splitOn "." (fromString str)) of
    Just (_qualif, _iden) -> QIden{..}
    Nothing               -> error "impossible"

data DataDef = DataDef {
  _name  :: Text
  , _rhs :: DataDefRhs
  }
 deriving (Show)

data ConstructorDef =
  ConstructorDef {
  _constructorName      :: Text
  , _constructorArgType :: Maybe LeafType
  }
 deriving (Show)

data DataDefRhs =
  Record {
     _fields :: [FieldDef]
     }
  | SumType {
      _constructors :: [ConstructorDef]
      }
  | Synonym {
      _synonymType :: LeafType
      }

 deriving (Show)

data PPX = Yojson | Show | ToYojson | FromYojson
  deriving (Show, Eq, Ord)

newtype Annotation = Annotation {
  ppxAnnotations :: [PPX]
  }
  deriving (Show)

data Statement =
  Open QualifiedIden
  | TopFunDef FunDef
  | TopDataDef DataDef
  | Deriving (Set PPX)
  | Verbatim Text
  deriving (Show)

type Iden = Text

newtype Literal = StringLit Text
  deriving (Show)

data MatchCase = MatchCase {
  _pattern    :: Pattern
  , _caseBody :: Expr
  }
  deriving (Show)

data Pattern =
  StringPat Text
  | Wildcard
  | PVar Iden
  | PConstructor Iden [Pattern]
  deriving (Show)

data Expr =
  Let Iden Expr Expr
  | Iden Iden
  | Match {
      _matchExpr :: Expr
     , _cases    :: [MatchCase]
     }
  | App Expr Expr
  | Literal Literal
  | List [Expr]
  | Tuple [Expr]
  deriving (Show)

data TypedIden = TypedIden Iden Type
  deriving (Show)

data FunDef = FunDef {
  _funName   :: Text
  , _context :: [TypedIden]
  , _type    :: Type
  , _body    :: Expr
  }
  deriving (Show)

newtype ModuleDef = ModuleDef {
  _statements :: [Statement]
  }
  deriving (Show)
