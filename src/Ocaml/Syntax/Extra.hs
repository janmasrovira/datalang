module Ocaml.Syntax.Extra (
  module Ocaml.Syntax.Extra
  , module Ocaml.Syntax
  ) where

import           Data.Text    (Text)
import           Ocaml.Syntax

infixl 0 $.
($.) :: Expr -> Expr -> Expr
($.) = App

infixl 9 @.
(@.) :: Expr -> Expr -> Expr
(@.) = App

textToType :: Text -> Type
textToType = Leaf . TyDataDef

leafTypeToText :: LeafType -> Text
leafTypeToText l = case l of
 TyInteger   -> "int"
 TyString    -> "string"
 TyFloat     -> "float"
 TyBool      -> "bool"
 TyDataDef t -> t
