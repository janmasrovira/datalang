module Ocaml.Syntax.Pretty where

import           Common
import qualified Data.Set                  as Set
import           Ocaml.Syntax
import           Prettyprinter
import           Prettyprinter.Render.Text

newtype Config = Config {
  _indent :: Int
  }

config :: Config
config = Config {
  _indent = 2
  }

prettyFieldDef :: FieldDef -> Doc a
prettyFieldDef FieldDef {..} =
  pretty _fieldName <+> colon <+> prettyType _fieldType

prettyLeafType :: LeafType -> Doc a
prettyLeafType t = case t of
  TyInteger   -> "int"
  TyString    -> "string"
  TyFloat     -> "float"
  TyBool      -> "bool"
  TyDataDef t -> pretty t

prettyType :: Type -> Doc a
prettyType t = case t of
  Leaf l   -> prettyLeafType l
  ListOf l -> prettyLeafType l <+> "list"

prettyConstructorDef :: ConstructorDef -> Doc a
prettyConstructorDef ConstructorDef{..} =
  pipe <+> pretty _constructorName <> constrType
  where
    constrType = case _constructorArgType of
      Nothing -> mempty
      Just ty -> space <> "of" <+> prettyLeafType ty

hang' :: Doc a -> Doc a
hang' = hang (_indent config)

sepBy :: Foldable t => Doc ann -> t (Doc ann) -> Doc ann
sepBy p = concatWith (\a b -> a <> p <> b)

prettyDataDefRhs :: DataDefRhs -> Doc a
prettyDataDefRhs d = case d of
  SumType{..} -> line <> vsep (map prettyConstructorDef _constructors)
  Record{..}  -> space <> braces (line <> sepBy (semi <> line) (map prettyFieldDef _fields) <> line)
  Synonym{..} -> space <> prettyLeafType _synonymType

prettyDataDef :: DataDef -> Doc a
prettyDataDef DataDef{..} =
  hang' $ "type" <+> pretty _name <+> equals <> prettyDataDefRhs _rhs

prettyTypedIden :: TypedIden -> Doc a
prettyTypedIden (TypedIden i ty) = parens (pretty i <+> colon <+> prettyType ty)

prettyLiteral :: Literal -> Doc a
prettyLiteral l = case l of
  StringLit t -> dquotes (pretty t)

prettyPattern :: Pattern -> Doc a
prettyPattern pat = case pat of
  Wildcard            -> "_"
  StringPat t         -> dquotes (pretty t)
  PVar v              -> pretty v
  PConstructor c []   -> pretty c
  PConstructor c args -> pretty c <+> hcat (map patParens args)
  where
   patParens :: Pattern -> Doc a
   patParens p
    | patNeedsParens p = parens (prettyPattern p)
    | otherwise = prettyPattern p

prettyMatchCase :: MatchCase -> Doc a
prettyMatchCase MatchCase{..} =
  pipe <+> prettyPattern _pattern <+> "->" <+> align (prettyExpr _caseBody)

patNeedsParens :: Pattern -> Bool
patNeedsParens p = case p of
  Wildcard          -> False
  PVar{}            -> False
  StringPat{}       -> False
  PConstructor _ [] -> False
  PConstructor{}    -> True

needsParens :: Expr -> Bool
needsParens e = case e of
  Iden{}    -> False
  Literal{} -> False
  List{}    -> False
  Tuple{}   -> False
  Let{}     -> True
  Match{}   -> True
  App{}     -> True

prettyPPX :: PPX -> Doc a
prettyPPX p = case p of
  Yojson     -> "yojson"
  ToYojson   -> "to_yojson"
  FromYojson -> "from_yojson"
  Show       -> "show"

prettyExpr :: Expr -> Doc a
prettyExpr expr = case expr of
 Iden i -> pretty i
 Literal l -> prettyLiteral l
 Let f fdef body ->
   align $ "let" <+> pretty f <+> equals <+> prettyExpr fdef <+> "in" <> line
   <> prettyExpr body
 Match{..} ->
   hang' $ "match" <+> prettyExpr _matchExpr <+> "with" <> line
    <> vsep (map prettyMatchCase _cases)
 App f x
   | needsParens x -> prettyExpr f <+> parens (prettyExpr x)
   | otherwise -> prettyExpr f <+> prettyExpr x
 List l -> encloseSep lbracket rbracket (semi <> space) (map prettyExpr l)
 Tuple l -> encloseSep lparen rparen (comma <> space) (map prettyExpr l)

prettyFunDef :: FunDef -> Doc a
prettyFunDef FunDef{..} =
  hang' $ "let" <+> pretty _funName <+> hsep (map prettyTypedIden _context)
  <+> colon <+> prettyType _type <+> equals <> line
  <> prettyExpr _body

prettyQualifiedIden :: QualifiedIden -> Doc a
prettyQualifiedIden QIden{..} =
  concatWith (\a b -> a <> dot <> b) (map pretty (_qualif ++ [_iden]))

prettyStatement :: Statement -> Doc a
prettyStatement s =
  case s of
    Open t       -> "open" <+> prettyQualifiedIden t
    TopDataDef d -> prettyDataDef d
    Deriving ts  -> encloseSep "[@@deriving " rbracket (comma <> space)
     (map prettyPPX (Set.toList ts))
    TopFunDef f  -> prettyFunDef f
    Verbatim txt  -> pretty txt

prettyStatements :: [Statement] -> Doc a
prettyStatements = snd . foldl' aux (Nothing, mempty)
  where
   aux :: (Maybe Statement, Doc a) -> Statement -> (Maybe Statement, Doc a)
   aux (Nothing, _) s = (Just s, prettyStatement s)
   aux (Just last, p) s = case (last, s) of
     (Open{}, Open{})           -> noLine
     (TopDataDef{}, Deriving{}) -> noLine
     (Deriving{}, Deriving{})   -> noLine
     (_, _)                     -> addLine
     where
       sep ln = (Just s, p <> ln <> prettyStatement s)
       addLine = sep (line <> line)
       noLine = sep line

prettyModuleDef :: ModuleDef -> Doc a
prettyModuleDef = prettyStatements . _statements

prettyModuleDefText :: ModuleDef -> Text
prettyModuleDefText = renderStrict . layoutPretty defaultLayoutOptions . prettyModuleDef
