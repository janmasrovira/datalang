module Latex.Extra (module Text.LaTeX
                   , module Latex.Extra
                   , module Text.LaTeX.Base.Class
                   , module Text.LaTeX.Base.Syntax) where

import           Common
import           Text.LaTeX
import           Text.LaTeX.Base.Class
import           Text.LaTeX.Base.Syntax

minipageenv :: [TeXArg] -> LaTeX -> LaTeX
minipageenv = TeXEnv "minipage"

textcolor :: Text -> LaTeX -> LaTeX
textcolor color txt = TeXComm "textcolor" [FixArg (raw color), FixArg txt]

(<+>) :: (Semigroup a, IsString a) => a -> a -> a
a <+> b = a <> " " <> b

sectionN' :: LaTeXC l => Int -> l -> l
sectionN' n = case n of
  2 -> section'
  3 -> subsection'
  4 -> subsubsection'
  _ -> error "wrong sectionN' depth"

sectionN :: LaTeXC l => Int -> l -> l
sectionN n = case n of
  1 -> chapter
  2 -> section
  3 -> subsection
  4 -> subsubsection
  _ -> error "wrong sectionN depth"

hyperlink :: LaTeX -> Text -> LaTeX
hyperlink capt lbl = TeXComm "hyperlink" [FixArg (raw lbl), FixArg capt]

hypertarget :: LaTeX -> Text -> LaTeX
hypertarget capt lbl = TeXComm "hypertarget" [FixArg (raw lbl), FixArg capt]

definecolor :: Text -> Text -> LaTeX
definecolor name hex = TeXComm "definecolor"
 [FixArg (fromText name), FixArg "HTML", FixArg (fromText hex)]

vline :: LaTeX
vline = TeXComm "vline" []

cmdAnd :: LaTeX
cmdAnd = TeXComm "and" []

subtitle :: LaTeX -> LaTeX
subtitle s = TeXComm "subtitle" [FixArg s]
