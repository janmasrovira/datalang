module DataLang.OrgParser where

import           Common
import           DataLang.Syntax (ModuleDef)
import           Pandoc.Compiler.DataLang
import           Text.Pandoc

parseOrgDataLang :: Text -> Either Text ModuleDef
parseOrgDataLang = mapLeft renderError . runPure . readOrg def >=> compile
