module DataLang.Compiler.Coq where

import           Common
import           Coq.Syntax.Extra
import           Data.Char             (isSpace)
import           Data.List.Extra       (snoc)
import           Data.Maybe
import qualified Data.Set              as Set
import           Data.Text             (Text, pack, strip)
import qualified DataLang.Syntax.Extra as D
import           Text.RawString.QQ

compileConstructorDef :: Text -> D.ConstructorDef -> ConstructorDef
compileConstructorDef tyName D.ConstructorDef {..} =
  ConstructorDef{..}
  where
    _constructorType :: Type
    _constructorType = constructorTypeFromArg tyName constructorArgType
    constructorArgType :: Maybe Type
    constructorArgType = case _constructorArg of
      D.None     -> Nothing
      D.Of t     -> Just (compileLeafType t)
      D.Fields{} -> Just (TyDataDef (wrapperName _constructorName))

compileLeafType :: D.LeafType -> Type
compileLeafType l = case l of
  D.TyInteger   -> TyNat
  D.TyString    -> TyString
  D.TyBool      -> TyBool
  D.TyFloat     -> error "Coq: float not supported"
  D.TyDataDef t -> TyDataDef t

compileType :: D.Type -> Type
compileType ty = case ty of
  D.Leaf t   -> compileLeafType t
  D.ListOf t -> TySeqOf (compileLeafType t)

compileFieldDef :: D.FieldDef -> FieldDef
compileFieldDef D.FieldDef{..} = FieldDef _fieldName (compileType _fieldType)

wrapperName :: Text -> Text
wrapperName constructorName = uncapitalize constructorName <> "_wrapper"

compileDataDef :: D.DataDef -> [Statement]
compileDataDef D.DataDef{..} = case _rhs of
 D.SumType{..} ->
   concatMap compileDataDef wrapperDefs
    ++ [def]
  where
   def :: Statement
   def = TopDataDef $ DataDef _name TyType $
    SumType (map (compileConstructorDef _name) _constructors)
   wrapperDefs :: [D.DataDef]
   wrapperDefs = mapMaybe wrapperDef _constructors
   wrapperDef :: D.ConstructorDef -> Maybe D.DataDef
   wrapperDef D.ConstructorDef{..} =
     case _constructorArg of
       D.None -> Nothing
       D.Of{} -> Nothing
       D.Fields fields ->
         Just (D.DataDef {
               _name = wrapperName _constructorName
               , _rhs = D.Record fields
               , _defDoc = _constructorDoc
               , _defAnnotation = _constructorAnnotation}
              )
 D.Record{..} -> [def]
   where
     def = TopDataDef $ DataDef _name TyType $
       Record ("mk_" <> _name) (map compileFieldDef _fields)
 D.Synonym{..} -> [def]
   where
     def = TopDataDef $ DataDef _name TyType $
       Synonym (compileLeafType _synonymType)

compileModule :: D.ModuleDef -> ModuleDef
compileModule m@D.ModuleDef{..} =
  ModuleDef $
    [Verbatim "From mathcomp Require Import all_ssreflect."
    , Verbatim "Require Import Coq.Strings.String."]
    ++
    concatMap compileDataDef (D._typeDefs m)
