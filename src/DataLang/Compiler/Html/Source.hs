module DataLang.Compiler.Html.Source (prettySource, prettySourceText) where

import           Common
import qualified Data.Text                               as T
import qualified Data.Text.Lazy                          as LT
import           DataLang.Syntax                         (ModuleDef)
import           DataLang.Syntax.Pretty
import           DataLang.Syntax.Pretty.Annotation
import qualified DataLang.Syntax.Utils                   as D
import           Html5.Extra
import           Prettyprinter
import           Prettyprinter.Render.Util.SimpleDocTree
import qualified Text.Blaze.Html.Renderer.Text           as Html
import           Text.Blaze.Html5                        as Html
import qualified Text.Blaze.Html5.Attributes             as Attr

prettySourceText :: String -> ModuleDef -> Text
prettySourceText baseName = LT.toStrict . Html.renderHtml . prettySource baseName

prettySource :: String -> ModuleDef -> Html
prettySource baseName m = docTypeHtml ! Attr.xmlns "http://www.w3.org/1999/xhtml" $
  mhead
  <> mbody
  where
  header :: Html
  header = Html.div ! Attr.id "package-header"
    $ (Html.span ! Attr.class_ "caption" $ "Source")
    <> (ul ! Attr.id "page-menu" ! Attr.class_ "links"
       $ mconcat (intersperse " · " menuItems))
  menuItems = [togg, goDoc]
    where
    togg = li $
     (input ! Attr.type_ "checkbox" ! Attr.id "toggle"
           ! Attr.onchange "toggle_visibility()")
     <> (label ! Attr.for "toggle" $ " Show Json")
    goDoc = Html.a ! Attr.href (fromString baseName <> "-doc.html") $ "Documentation"
  mhead :: Html
  mhead =
    livejs
    <> metaUtf8
    <> sourceCss
    <> highlightJs
  src :: Html
  src = (pre ! Attr.id "src-content") .
        go . treeForm . layoutPretty defaultLayoutOptions . prettyModuleDef $ m
  mbody :: Html
  mbody =
    header
    <> src

renderTree :: SimpleDocTree Ann -> Html
renderTree = go

go :: SimpleDocTree Ann -> Html
go sdt = case sdt of
    STEmpty           -> mempty
    STChar c          -> toHtml c
    STText _ t        -> toHtml t
    STLine i          -> Html.span "\n" <> Html.span (fromText $ textSpaces i)
    STAnn ann content -> putTag ann (go content)
    STConcat contents -> mconcatMap go contents

tooltip :: Attribute
tooltip = Attr.class_ "tooltip"

tooltiptext :: Attribute
tooltiptext = Attr.class_ "tooltiptext"

putTag :: Ann -> Html -> Html
putTag sh x = case sh of
  Keyword       -> tagkw x
  Keysym        -> tagks x
  Json j k      -> tagJson j k x
  IdenRef k lab -> tagRef lab k x
  IdenDef k lab -> tagDef lab k x
  TypeDefSep    -> br <> x
  Rhs           -> x
  RhsItem       -> x
  DataDef       -> x
  FunDef        -> x
  SepFieldSemi  -> x
  Tooltip tt    -> tagTooltip tt x

tagkw :: Html -> Html
tagkw = Html.span ! Attr.class_ "hs-keyword"

tagJson :: JsonKind -> Kind -> Html -> Html
tagJson j k x = (Html.div ! Attr.class_ "details-json")
  (details $
   summ
   <> (Html.p ! Attr.class_ "json-body" $ x))
  where
  summ :: Html
  summ = summary ! Attr.class_ "json-summary"
   $ case j of
      KRaw -> "RawJson"
      _    -> "SchemaJson"

tagks :: Html -> Html
tagks = Html.span ! Attr.class_ "hs-glyph"

tagTooltip :: Text -> Html -> Html
tagTooltip tt x = (Html.div ! tooltip) (x <> (Html.span ! tooltiptext) (toHtml tt))

tagRef :: Text -> Kind -> Html -> Html
tagRef t k x = Html.span ! Attr.class_ "annot"
   $ a ! Attr.class_ "" ! Attr.href (D.selfLinkName t)
   $ Html.span ! Attr.class_ cl
   $ x
  where
  cl = case k of
    KType -> "hs-identifier hs-type"
    _     -> "hs-identifier hs-var"


tagDef :: Text -> Kind -> Html -> Html
tagDef t k x = Html.span ! Attr.id (D.tagIden t)
   $ tagRef t k x

renderHtml :: SimpleDocStream Ann -> Text
renderHtml =  LT.toStrict . Html.renderHtml . renderTree . treeForm

css :: Html
css = toHtml $ T.strip [r|
|]
