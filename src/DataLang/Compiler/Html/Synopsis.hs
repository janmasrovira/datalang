module DataLang.Compiler.Html.Synopsis (prettySynopsis) where

import           Common
import qualified Data.Text                               as T
import qualified Data.Text.Lazy                          as LT
import           DataLang.Syntax                         (ModuleDef)
import           DataLang.Syntax.Pretty
import           DataLang.Syntax.Pretty.Annotation
import           Prettyprinter
import           Prettyprinter.Render.Util.SimpleDocTree
import qualified Text.Blaze.Html.Renderer.Text           as Html
import           Text.Blaze.Html5                        as Html
import qualified Text.Blaze.Html5.Attributes             as Attr

prettySynopsis :: ModuleDef -> Html
prettySynopsis = go . treeForm . layoutPretty defaultLayoutOptions . prettyModuleDef

renderTree :: SimpleDocTree Ann -> Html
renderTree = go

go :: SimpleDocTree Ann -> Html
go sdt = case sdt of
    STEmpty           -> mempty
    STChar c          -> toHtml c
    STText _ t        -> toHtml t
    STLine i          -> mempty
    STAnn ann content -> putTag ann (go content)
    STConcat contents -> mconcatMap go contents

putTag :: Ann -> Html -> Html
putTag sh x = case sh of
  r -> case r of
    DataDef      -> li ! Attr.class_ "src short" $ x
    FunDef       -> li ! Attr.class_ "src short" $ x
    Rhs          -> ul ! Attr.class_ "subs" $ x
    RhsItem      -> li x
    Tooltip t    -> x
    Keyword      -> Html.span ! Attr.class_ "keyword" $ x
    Keysym       -> x
    SepFieldSemi -> mempty
    IdenRef k t  -> tagRef t k x
    IdenDef k t  -> a ! Attr.href (fromText $ refTagIden t) $ x
    TypeDefSep   -> x
    Json{}       -> mempty

tagRef :: Text -> Kind -> Html -> Html
tagRef t k x = case k of
  r -> case r of
    KField         -> a ! Attr.href (fromText $ refTagIden t) $ x
    KType          -> a ! Attr.href (fromText $ refTagIden t) $ x
    KConstructor   -> a ! Attr.href (fromText $ refTagIden t) $ x
    KFun           -> a ! Attr.href (fromText $ refTagIden t) $ x
    KArg           -> a ! Attr.href (fromText $ refTagIden t) $ x
    KPrimitiveType -> x

tagIden :: Text -> Text
tagIden t = "t:" <> t

refTagIden :: Text -> Text
refTagIden t = "#" <> tagIden t
