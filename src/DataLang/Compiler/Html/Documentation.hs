module DataLang.Compiler.Html.Documentation where

import           Common
import           Control.Lens
import qualified Data.HashMap.Strict             as HashMap
import qualified Data.Text                       as Text
import qualified Data.Text.IO                    as Text
import qualified Data.Text.Lazy                  as LT
import           DataLang.Syntax.Utils
import           DataLang.Compiler.Html.Source
import           DataLang.Compiler.Html.Synopsis
import           DataLang.Syntax.Extra
import           DataLang.Syntax.Pretty
import           Html5.Extra
import           Pandoc.Compiler.DataLang
import           Pandoc.Extra
import qualified Pandoc.Lens                     as L
import           Text.Blaze
import qualified Text.Blaze.Html.Renderer.Text   as Html
import           Text.Blaze.Html5                as Html hiding (map)
import qualified Text.Blaze.Html5.Attributes     as Attr
import qualified Text.Pandoc                     as P
import           Text.Pandoc.Options             (def)
import           Text.Pandoc.Walk
import           Text.Pandoc.Writers.HTML

compileModuleHtmlText :: String -> ModuleDef -> Text
compileModuleHtmlText baseName = LT.toStrict . Html.renderHtml . compileModule baseName

-- | This function compiles a datalang module into Html documentation.
compileModule ::
  -- | Base name
  String -> ModuleDef -> Html
compileModule baseName m = docTypeHtml $
  mhead
  <> mbody
  where
 m'@ModuleDef{..} = preprocess m
 titleStr = toHtml $ fromMaybe "Documentation" (HashMap.lookup "Title" _meta)
 mhead :: Html
 mhead = Html.head $
  title titleStr
  <> Html.meta ! Attr.httpEquiv "Content-Type"
               ! Attr.content "text/html; charset=UTF-8"
  <> Html.meta ! Attr.name "viewport"
               ! Attr.content "width=device-width, initial-scale=1"
  <> mathJaxCdn
  <> livejs
  <> linuwialCss
 mbody :: Html
 mbody = body ! Attr.class_ "js-enabled"
  $ packageHeader
    <> content
    <> footer
 packageHeader :: Html
 packageHeader = mempty
 footer :: Html
 footer = mempty
 content :: Html
 content =
  Html.div ! Attr.id "content" $
     moduleHeader
  <> toc
  <> docPreface
  <> synopsis
  <> interface
 synopsis :: Html
 synopsis = Html.div ! Attr.id "synopsis"
   $ details ! Attr.id "syn"
   $ summary "Synopsis"
     <> (ul ! Attr.class_ "details.toggle"
            ! dataAttribute "details-id" "syn"
        $ prettySynopsis m')
 docPreface :: Html
 docPreface = Html.div ! Attr.id "description"
   $ Html.div ! Attr.class_ "doc"
   $ compilePandoc _documentation
 tocEntries :: Html
 tocEntries = go (parsePandocForest' (_documentation ^. L.body))
   where
   go :: PandocForest -> Html
   go PandocForest{..} = ul (mconcatMap goh _sections)
   goh :: (HeaderNode, PandocForest) -> Html
   goh (HeaderNode _level _attrs _inlines, f) =
     case _inlines of
     [Span (sid,c1,c2) l] ->
       li $
       (a ! Attr.href (fromText (Text.cons '#' sid))
          $ compileInlines l)
       <> go f
     _ -> error "badly numbered header"
 toc :: Html
 toc = Html.div ! Attr.id "table-of-contents"
   $ Html.div ! Attr.id "contents-list"
   $ (p ! Attr.class_ "caption"
       ! Attr.onclick "window.scrollTo(0,0)"
       $ "Contents"
       )
      <> tocEntries
 moduleHeader :: Html
 moduleHeader = Html.div ! Attr.id "module-header" $
  (table ! Attr.class_ "info"
   $ tbody (mconcatMap (uncurry metaLine) (HashMap.toList _meta)))
   <> (p ! Attr.class_ "caption" $ titleStr)
   where
   metaLine :: Text -> Text -> Html
   metaLine k v = tr (th (toHtml k) <> td (toHtml v))
 interface :: Html
 interface = Html.div ! Attr.id "interface" $
  Html.h1 "Definitions"
  <> mconcatMap compileDataDef _typeDefs
  <> mconcatMap compileFunDef _functionDefs

 compileBlocks :: [Block] -> Html
 compileBlocks = compilePandoc . Pandoc mempty

 compileInlines :: [Inline] -> Html
 compileInlines = compilePandoc . Pandoc mempty . pure . Para

 labelHeaders :: ModuleDef -> ModuleDef
 labelHeaders m =
   m & documentation . L.body %~ aux
   where
   aux :: [Block] -> [Block]
   aux b = flattenForest (evalState (go $ forest b) 1)
   forest :: [Block] -> PandocForest
   forest b = fromMaybe (error "labelHeaders") $ parsePandocForest b
   go :: PandocForest -> State Int PandocForest
   go (PandocForest _preface _sections)  = PandocForest _preface <$> mapM goh _sections
   goh :: (HeaderNode, PandocForest) -> State Int (HeaderNode, PandocForest)
   goh (hn, f) = do
    i <- get
    let hn' = hn & inlines %~ addSpan i
    modify (+1)
    f' <- go f
    return (hn', f')
   addSpan :: Int -> [Inline] -> [Inline]
   addSpan i l = [Span (headerIdPrefix <> showT i, mempty, mempty) l]

 headerIdPrefix :: Text
 headerIdPrefix = "g:"

 compilePandocBlock :: Pandoc -> Html
 compilePandocBlock d =
  fromRight (error "compileBlock: pandoc error")
                    $ runPure (writeHtml5 def d)

 headerIden :: Text -> Html
 headerIden funName =
   a ! Attr.id (tagIden funName)
     ! Attr.class_ "def"
     $ toHtml funName

 compileFunArgIdentifierDef :: Text -> Text -> Html
 compileFunArgIdentifierDef fun arg =
   a ! Attr.id (fromText $ tagIden (qualifyFunArg fun arg))
     $ toHtml arg

 compileFunDef :: FunDef -> Html
 compileFunDef f@FunDef{..} = Html.div ! Attr.class_ "top" $
   functionHeader
   <> subsArguments
   <> docPart _funDefDoc
  where
   hasSignature :: Bool
   hasSignature = not (any hasDoc _funArgs)
     where hasDoc :: FunArg -> Bool
           hasDoc = (/=) mempty . _argDoc
   subsArguments :: Html
   subsArguments
    | hasSignature = mempty
    | otherwise = p ! Attr.class_ "subs arguments" $
      table $ tbody $ compileArgs _funArgs <> compileRet _funRet
      where
      compileArgs :: [FunArg] -> Html
      compileArgs = mconcatMap compileArgLine . zip (":" : repeat "→")
      compileArgLine :: (Html, FunArg) -> Html
      compileArgLine (prefix, arg@FunArg{..}) =
        tr $
           (td ! Attr.class_ "src" $ prefix
            <> compileArg _funName arg) <> (td ! Attr.class_ "doc" $ compileBlocks _argDoc)
      compileRet :: Type -> Html
      compileRet ret =
        tr $
           (td ! Attr.class_ "src" $ "→ " <> compileType ret)
        <> (td ! Attr.class_ "doc empty" $ mempty)

   -- | This method is used when none of the arguments has specific documentation.
   functionHeader :: Html
   functionHeader =
    p ! Attr.class_ "src" $
      (Html.span ! aKeyword) "fun "
        <> headerIden _funName
        <> typeSig
        <> sourceAndSelfLink _funName
     where
     typeSig :: Html
     typeSig
       | hasSignature = " : " <> mconcat (intersperse " → " (map (compileArg _funName) _funArgs <> [compileType _funRet]))
       | otherwise = mempty

 compileArg :: Text -> FunArg -> Html
 compileArg funName FunArg{..} = case _argName of
   Just arg -> " (" <> compileFunArgIdentifierDef funName arg <> " : " <> compileType _argType <> ")"
   Nothing -> compileType _argType


 sourceAndSelfLink :: Text -> Html
 sourceAndSelfLink name = (a ! Attr.href (linkSrcName name)
             ! Attr.class_ "link"
             $ "Source")
       <> (a ! Attr.href (selfLinkName name)
             ! Attr.class_ "selflink"
             $ "#")

 docPart :: [Block] -> Html
 docPart pand = Html.div ! Attr.class_ "doc"
     $ compileBlocks pand

 compileDataDef :: DataDef -> Html
 compileDataDef d@DataDef{..} = Html.div ! Attr.class_ "top" $
   compileDataDefHeader d
   <> docPart _defDoc
   <> rhsPart
   where
   rhsPart = compileDataDefRhs _rhs

 compileDataDefHeader :: DataDef -> Html
 compileDataDefHeader DataDef{..} =
   p ! Attr.class_ "src" $
     (Html.span ! aKeyword) "type "
       <> headerIden _name
       <> maybeSynonym
       <> sourceAndSelfLink _name
   where
     maybeSynonym = case _rhs of
       Synonym{..} -> " = " <> compileLeafType _synonymType
       _           -> mempty

 compileDataDefRhs :: DataDefRhs -> Html
 compileDataDefRhs d = case d of
   Record{..} -> compileFields _fields
   SumType{..} -> Html.div ! Attr.class_ "subs constructors"
     $ (p ! Attr.class_ "caption" $ "Constructors")
      <> compileConstructors _constructors
   Synonym{..} -> mempty

 compileConstructor :: ConstructorDef -> Html
 compileConstructor ConstructorDef{..} =
   tr (srcPart <> docPart)
   <> compileConstructorArg _constructorArg
   where
     docPart = td ! Attr.class_ "doc"
                  $ compileBlocks _constructorDoc
     srcPart = td ! Attr.class_ "src" $
       a ! Attr.id (constructorAttrId _constructorName)
         ! Attr.class_ "def"
         $ toHtml _constructorName

 fieldAttrId :: IsString a => Text -> a
 fieldAttrId fieldName = fromText $ tag <> fieldName
   where tag = "t:"

 constructorAttrId :: IsString a => Text -> a
 constructorAttrId constrName = fromText $ tag <>  constrName
   where tag = "t:"

 compileConstructorArg :: ConstructorArg -> Html
 compileConstructorArg a = case a of
   None      -> mempty
   Of l      -> mempty
   Fields fs -> compileFields fs

 compileFields :: [FieldDef] -> Html
 compileFields fs = tr
   $ td ! Attr.colspan "2"
   $ Html.div ! Attr.class_ "subs fields"
   $ ul (mconcatMap compileField fs)

 compileField :: FieldDef -> Html
 compileField FieldDef{..} = li $ srcPart <> docPart
   where
     docPart = compileBlocks _fieldDoc
     srcPart = Html.dfn ! Attr.class_ "src"
      $ (a ! Attr.id (fieldAttrId _fieldName)
           ! Attr.class_ "def"
           $ toHtml _fieldName)
      <> " :: "
      <> compileType _fieldType

 compileType :: Type -> Html
 compileType ty = case ty of
   Leaf l   -> compileLeafType l
   ListOf l -> "list of " <> compileLeafType l

 compileLeafType :: LeafType -> Html
 compileLeafType ty = case ty of
   TyDataDef tyName -> a ! Attr.href (selfLinkName tyName)
                         $ toHtml tyName
   TyInteger -> "int"
   TyString -> "string"
   TyFloat -> "float"
   TyBool -> "bool"

 compileConstructors :: [ConstructorDef] -> Html
 compileConstructors = table . tbody . mconcatMap compileConstructor

 linkSrcName :: IsString a => Text -> a
 linkSrcName x = fromText $ (fromString baseName <> "-src.html#") <> x

 selfLinkName :: IsString a => Text -> a
 selfLinkName x = fromText $ "#t:" <> x

 aKeyword :: Attribute
 aKeyword = Attr.class_ "keyword"

 compilePandoc :: Pandoc -> Html
 compilePandoc = fromRight (error "compilePandoc Pandoc error") .
    runPure . writeHtml5 def

 preprocess :: ModuleDef -> ModuleDef
 preprocess = labelHeaders . addLinks
