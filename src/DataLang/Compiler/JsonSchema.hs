module DataLang.Compiler.JsonSchema where

import           Common
import           Data.Aeson
import           Data.Aeson.Encode.Pretty
import           Data.ByteString.Lazy     (toStrict)
import qualified Data.HashSet             as HashSet
import           DataLang.Syntax.Extra    as D
import           JsonSchema.Builder
import           JsonSchema.Syntax        as Schema
import           JsonSchema.Syntax.Pretty

data CompileError =
 MissingDef Text
 | DuplicateDef Text
 | PopEmpty
 | EmptyStack
 | BuildError SchemaError
 deriving (Show)

class IsCompileError c where
  fromCompileError :: CompileError -> c

instance IsCompileError CompileError where
  fromCompileError = id

instance IsCompileError Text where
  fromCompileError = pack . show

instance IsSchemaError CompileError where
  fromBuildError = BuildError

data CompileEnv = CompileEnv {
  _defsTable           :: DefsTable
  , _missingDefIsError :: Bool
  }

type DefsTable = HashSet Text

asksDefsTable :: MonadReader CompileEnv m => (DefsTable -> a) -> m a
asksDefsTable f = asks (f . _defsTable)

compileType :: (MonadError CompileError m, MonadReader CompileEnv m) => Type -> m JsonType
compileType t = case t of
  D.ListOf{} -> return TyArray
  D.Leaf l   -> compileLeafType l

compileLeafType :: (MonadError CompileError m, MonadReader CompileEnv m) => LeafType -> m JsonType
compileLeafType t = case t of
  D.TyInteger      -> return Schema.TyInteger
  D.TyString       -> return Schema.TyString
  D.TyBool         -> return Schema.TyBoolean
  D.TyFloat        -> return Schema.TyNumber
  D.TyDataDef name -> do
    whenM (andM [asks _missingDefIsError
                , asksDefsTable (notElem name)])
     (throwError (MissingDef name))
    return $ Schema.TyRef name

compileDefCurrentBlock :: forall m. (MonadError CompileError m, MonadState SchemaBuilder m, MonadReader CompileEnv m)
  => DataDef -> m ()
compileDefCurrentBlock DataDef{..} = do
    rhsNodes
    annotation _defAnnotation
  where
 tagStr = "tag"
 contentsStr = "contents"
 auxFields :: [FieldDef] -> m ()
 auxFields _fields = do
   requiredFields
   additionalProperties False
   fieldsProperties
    where
      describeField :: FieldDef -> m (Text, m ())
      describeField FieldDef{..} = do
        ct <- compileType _fieldType
        return (_fieldName, do
         annotation _fieldAnnotation
         type_ ct)
      requiredFields :: m ()
      requiredFields = required (map _fieldName _fields)
      fieldsProperties :: m ()
      fieldsProperties = mapM describeField _fields >>= properties
 rhsNodes :: m ()
 rhsNodes = case _rhs of
  Synonym{..}  -> do
    compileLeafType _synonymType >>= type_
    annotation _synonymAnnotation
  Record{..}  -> do
   type_ TyObject
   auxFields _fields
  SumType{..} -> do
   type_ TyObject
   required [tagStr]
   properties [(tagStr, enum $ map (String . _constructorName) _constructors)]
   allOfMaybe (mapMaybe contentsType _constructors)
   where
     contentsType :: ConstructorDef -> Maybe (m ())
     contentsType ConstructorDef{..} = case _constructorArg of
       None -> Just (annotation _constructorAnnotation)
       Fields fields -> Just fieldsProperties
        where
          fieldsProperties :: m ()
          fieldsProperties = do
            annotation _constructorAnnotation
            ifThen
              (properties [(tagStr, const_ (String _constructorName))])
              (auxFields fields)
       Of ty -> Just $ do
         annotation _constructorAnnotation
         ifThen
           (properties [(tagStr, const_ (String _constructorName))])
           (do
             required [contentsStr]
             ty' <- compileLeafType ty
             properties [(contentsStr, type_ ty')])

compileModule_ :: forall m. (MonadError CompileError m, MonadState SchemaBuilder m, MonadReader CompileEnv m)
  => ModuleDef -> m Schema
compileModule_ m@ModuleDef{..} =
  case unsnoc _typeDefs of
    Nothing -> withBlock (return ())
    Just (_otherDefs, _mainDef) ->
     withBlock $ do
     schema uri2019
     defsMaybe $ map (_name &&& compileDefCurrentBlock) _otherDefs
     compileDefCurrentBlock _mainDef

getDefsTable :: forall m. MonadError CompileError m => ModuleDef -> m DefsTable
getDefsTable = foldM addDef mempty . _typeDefs
  where
    addDef :: DefsTable -> DataDef -> m DefsTable
    addDef t DataDef{..}
      | HashSet.member _name t = throwError (DuplicateDef _name)
      | otherwise = return $ HashSet.insert _name t

compileModule :: IsCompileError e => ModuleDef -> Either e Schema
compileModule m = mapLeft fromCompileError $
  runExcept $ do
  table <- getDefsTable m
  (`runReaderT` CompileEnv {
      _defsTable = table
      , _missingDefIsError = True
      })
    $ (`evalStateT` SchemaBuilder mempty)
    $ compileModule_ m

prettyError :: CompileError -> Text
prettyError = pack . show

compileModuleText :: ModuleDef -> Text
compileModuleText =
  either prettyError prettyJson . compileModule
