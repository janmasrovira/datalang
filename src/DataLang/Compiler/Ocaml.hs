module DataLang.Compiler.Ocaml where

import           Common
import           Data.Char             (isSpace)
import           Data.List.Extra       (snoc)
import           Data.Maybe
import qualified Data.Set              as Set
import           Data.Text             (Text, pack, strip)
import qualified DataLang.Syntax.Extra as D
import           Ocaml.Syntax.Extra
import           Text.RawString.QQ

compileConstructorDef :: D.ConstructorDef -> ConstructorDef
compileConstructorDef D.ConstructorDef {..} =
  ConstructorDef{..}
  where
    _constructorArgType :: Maybe LeafType
    _constructorArgType = case _constructorArg of
      D.None     -> Nothing
      D.Of t     -> Just (compileLeafType t)
      D.Fields{} -> Just (TyDataDef (wrapperName _constructorName))

compileLeafType :: D.LeafType -> LeafType
compileLeafType l = case l of
  D.TyInteger   -> TyInteger
  D.TyBool      -> TyBool
  D.TyString    -> TyString
  D.TyFloat     -> TyFloat
  D.TyDataDef t -> TyDataDef t

compileType :: D.Type -> Type
compileType ty = case ty of
  D.Leaf t   -> Leaf (compileLeafType t)
  D.ListOf t -> ListOf (compileLeafType t)

compileFieldDef :: D.FieldDef -> FieldDef
compileFieldDef D.FieldDef{..} = FieldDef _fieldName (compileType _fieldType)

wrapperName :: Text -> Text
wrapperName constructorName = uncapitalize constructorName <> "_wrapper"

genDataDefParser :: Text -> [D.ConstructorDef] -> Statement
genDataDefParser _name _constructors =
  TopFunDef $
    FunDef (_name <> "_of_yojson") [TypedIden js type_t] (textToType _name)
    (Let tag (to_string $. member @. ltag @. ijs)
     (Match itag cases))
   where
    cases = map parConstructorCase _constructors ++ [impossibleCase]

parConstructorCase :: D.ConstructorDef -> MatchCase
parConstructorCase D.ConstructorDef{..} = MatchCase (StringPat _constructorName) $
  case _constructorArg of
   D.None -> constr
   D.Of ty -> Let contents (ty_of_yojson @. (member @. lcontents @.ijs))
     (constr @. icontents)
     where ty_of_yojson = Iden (leafTypeToText (compileLeafType ty) <> "_of_yojson")
   D.Fields{} -> Let contents (ty_of_yojson @. (deleteTag @. ijs))
    (constr @. icontents)
     where ty_of_yojson = Iden (wrapperName _constructorName <> "_of_yojson")
   where
   constr = Iden _constructorName

genDataDefSerializer :: Text -> [D.ConstructorDef] -> Statement
genDataDefSerializer _name _constructors =
   TopFunDef $
   FunDef ("yojson_of_" <> _name) [TypedIden a (textToType _name)] type_t
   (Match ia cases)
    where
     cases = map constructorCase _constructors
     constructorCase :: D.ConstructorDef -> MatchCase
     constructorCase D.ConstructorDef{..} =
       case _constructorArg of
         D.None -> MatchCase (PConstructor _constructorName [])
                     (assoc @. List [Tuple [ltag, tstring (strLit _constructorName)]])
         D.Of argTy ->
           MatchCase (PConstructor _constructorName [parg])
            (assoc @. List [
             Tuple [ltag, tstring (strLit _constructorName)]
            , Tuple [lcontents,
                     Iden ("yojson_of_" <> leafTypeToText (compileLeafType argTy)) @. iarg]])
         D.Fields{}  ->
           MatchCase (PConstructor _constructorName [parg])
            (addTag @. strLit _constructorName @.
             (Iden ("yojson_of_" <> wrapperName _constructorName) @. iarg))


compileDataDef :: D.DataDef -> [Statement]
compileDataDef D.DataDef{..} = case _rhs of
 D.SumType{..} ->
   concatMap compileDataDef wrapperDefs
    ++ [def
       , genDataDefParser _name _constructors
       , genDataDefSerializer _name _constructors]
  where
   def :: Statement
   def = TopDataDef $ DataDef _name $
    SumType (map compileConstructorDef _constructors)
   wrapperDefs :: [D.DataDef]
   wrapperDefs = mapMaybe wrapperDef _constructors
   wrapperDef :: D.ConstructorDef -> Maybe D.DataDef
   wrapperDef D.ConstructorDef{..} =
     case _constructorArg of
       D.None -> Nothing
       D.Of{} -> Nothing
       D.Fields fields ->
         Just (D.DataDef {
               _name = wrapperName _constructorName
               , _defDoc = _constructorDoc
               , _rhs = D.Record fields
               , _defAnnotation = _constructorAnnotation}
              )
 D.Record{..} -> [def
                 , Deriving (Set.fromList [Yojson])]
   where
     def = TopDataDef $ DataDef _name $
       Record (map compileFieldDef _fields)
 D.Synonym{..} -> [def
                 , Deriving (Set.fromList [Yojson])]
   where
     def = TopDataDef $ DataDef _name $
       Synonym (compileLeafType _synonymType)


compileModule :: D.ModuleDef -> ModuleDef
compileModule m@D.ModuleDef{..} =
  ModuleDef $
    [Open (QIden ["Yojson", "Safe"] "Util")
    , Open (QIden ["Yojson"] "Safe")]
    ++
    [extraDefs]
    ++
    concatMap compileDataDef (D._typeDefs m)

extraDefs :: Statement
extraDefs = Verbatim $ strip $ pack
  [r|
let rec delete_tag_rec (l : (string * t) list) : (string * t) list =
  match l with
   | [] -> []
   | ("tag", _) :: ls -> ls
   | h :: ls -> h :: delete_tag_rec ls

let delete_tag (js : t) : t =
  match js with
   | `Assoc ls -> `Assoc (delete_tag_rec ls)
   | x -> x

let add_tag (tag : string) (js : t) : t =
  match js with
   | `Assoc l -> `Assoc (("tag", `String tag) :: l)
   | _ -> failwith "add_tag: argument must be an object"
|]


-- Some variables and small parts which are used in the parser and serializator
-- generation.
icontents = Iden contents
tag = "tag"
ltag = Literal (StringLit tag)
lcontents = Literal (StringLit contents)
itag = Iden tag
to_string = Iden "to_string"
member = Iden "member"
deleteTag = Iden "delete_tag"
addTag = Iden "add_tag"
contents = "contents"
js = "js"
a = "a"
ia = Iden a
ijs = Iden js
type_t = textToType "t"
arg = "arg"
parg = PVar arg
iarg = Iden arg
assoc = Iden "`Assoc"
tstring = App (Iden "`String")
strLit = Literal . StringLit
unrecognizedTag = Iden "failwith" @. Literal (StringLit "unrecognized tag")
impossibleCase = MatchCase Wildcard unrecognizedTag
