module DataLang.Compiler.Latex where

import           Common
import qualified Data.HashMap.Strict                     as HashMap
import qualified Data.Text                               as Text
import           DataLang.Syntax.Extra
import qualified DataLang.Syntax.Pretty                  as PP
import qualified DataLang.Syntax.Pretty.Latex            as L
import           DataLang.Syntax.Utils
import           Latex.Extra
import qualified Prettyprinter                           as PP
import           Prettyprinter.Render.Util.SimpleDocTree
import           Text.LaTeX
import           Text.LaTeX.Base.Class
import           Text.LaTeX.Base.Syntax
import           Text.LaTeX.Packages.Hyperref
import           Pandoc.Extra                             hiding (report)
import qualified Text.Pandoc                             as P
import           Text.Pandoc.Options
import           Text.Pandoc.Writers.LaTeX


compileModuleText :: ModuleDef -> Text
compileModuleText = render . compileModule

-- | Note that the "\n" after \end{Verbatim} is mandatory.
codeBlock :: LaTeX -> LaTeX
codeBlock x =
  "\n" <>
  TeXEnv "Verbatim" [MOptArg [raw "commandchars=\\\\\\{\\}"
                    , raw "frame=single"]
                    ] ("\n" <> x <> "\n")
  <> "\n"

compileModule :: ModuleDef -> LaTeX
compileModule m = documentclass [] "scrreprt"
  <> preface
  <> pandocTemplateExtra
  <> document (
     titlePage
     <> tableofcontents
     <> body)
  where
  ModuleDef {..} = addLinks m
  meta = case _documentation of
    Pandoc m _ -> m
  titleInlines = docTitle meta
  titlePage :: LaTeX
  titlePage
   | null titleInlines = mempty
   | otherwise =
    setAuthors
    <> setTitle
    <> setSubtitle
    <> setDate
    <> maketitle
    where
    setAuthors = author $ mconcat $ intersperse cmdAnd $ map compileInlines $ docAuthors' meta
    setTitle = title $ compileInlines titleInlines
    setDate
      | null dt = mempty
      | otherwise = date $ compileInlines dt
     where
       dt = docDate meta
    setSubtitle = subtitle $ compileInlines $ docSubtitle meta
  body = sectionN 1 "Preface"
   <> compilePandoc _documentation
   <> definitions
    where
     definitions = sectionN 1 "Definitions"
      <> mconcatMap compileDataDef _typeDefs
      <> mconcatMap compileFunDef _functionDefs
  preface = imports <> extraPreface
  imports =
    usepackage [] hyperref
    <> usepackage [] "mathtools"
    <> usepackage ["margin=2.6cm"] "geometry"
    <> usepackage ["x11names","table"] "xcolor"
    <> usepackage [] "amssymb"
    <> usepackage [] "unicode-math"
    <> usepackage [] "longtable"
    <> usepackage [] "booktabs"
    <> usepackage [] "fancyvrb"
    <> usepackage [] "graphicx"
    <> usepackage ["normalem"] "ulem"
    <> usepackage [] "iftex"
    <> usepackage [] "float"

compilePandoc :: Pandoc -> LaTeX
compilePandoc = raw . fromRight' . runPure . writeLaTeX def

compileBlocks :: [Block] -> LaTeX
compileBlocks = compilePandoc . Pandoc mempty

compileInlines :: [Inline] -> LaTeX
compileInlines = compilePandoc . Pandoc mempty . pure . Plain

compileFunDef :: FunDef -> LaTeX
compileFunDef def@FunDef{..} =
  sectionN 2 (texttt ("fun" <+> fromText _funName))
  <> labelIden _funName
  <> sig
  <> compileBlocks _funDefDoc
  where
  sig = codeBlock . L.renderTree . treeForm
    . PP.layoutPretty PP.defaultLayoutOptions . PP.prettyFunDef $ def

compileDefRhs :: Text -> DataDefRhs -> LaTeX
compileDefRhs name d = case d of
  Record fields -> sectionN' 3 "Fields" <+>
    compileFields False fields
  SumType constrs -> sectionN' 3 "Constructors" <+>
    compileConstructors constrs
  Synonym{..} -> mempty

compileDataDef :: DataDef -> LaTeX
compileDataDef def@DataDef{..} =
  sectionN 2 (texttt ("type" <+> fromText _name))
  <> labelIden _name
  <> source
  <> compileBlocks _defDoc
  <> compileDefRhs _name _rhs
  where
  source = codeBlock . L.renderTree . treeForm
    . PP.layoutPretty PP.defaultLayoutOptions . PP.prettyDataDef $ def

labelIden :: Text -> LaTeX
labelIden = hypertarget "" . tagIden

compileLeafType :: LeafType -> LaTeX
compileLeafType lt = texttt $ case lt of
  TyDataDef name -> fromText name
  TyInteger      -> "int"
  TyString       -> "string"
  TyFloat        -> "float"
  TyBool         -> "bool"

compileType :: Type -> LaTeX
compileType t = case t of
 Leaf lt   -> compileLeafType lt
 ListOf lt -> texttt "list of" <+> compileLeafType lt

compileField :: FieldDef -> LaTeX
compileField FieldDef{..} =
  labelIden _fieldName
  <+> compileBlocks _fieldDoc

compileFields :: Bool -> [FieldDef] -> LaTeX
compileFields constructor f =
  vertBarLeft spc (description . mconcatMap aux $ f)
  where
  spc
   | constructor = 0.12
   | otherwise = 0.06
  aux :: FieldDef -> LaTeX
  aux f@FieldDef {..} =
    item (Just (fieldDef _fieldName <+> texttt ":"
                <+> compileType _fieldType <> "."))
    <> compileField f

constructorDef :: Text -> LaTeX
constructorDef = textcolor constructorColorStr . texttt . fromText

fieldDef :: Text -> LaTeX
fieldDef = textcolor fieldColorStr . texttt . fromText

compileConstructors :: [ConstructorDef] -> LaTeX
compileConstructors = vertBarLeft 0.06 . description . mconcatMap aux
  where
    aux :: ConstructorDef -> LaTeX
    aux f@ConstructorDef{..} =
      item (Just (constructorDef _constructorName))
      <> compileConstructorDescr f
      <> compileConstructorArg _constructorArg

compileConstructorArg :: ConstructorArg -> LaTeX
compileConstructorArg c = case c of
  None       -> mempty
  Of lt      -> compileLeafType lt
  Fields fds -> hfill <> raw "\\break{}" <> compileFields True fds

compileConstructorDescr :: ConstructorDef -> LaTeX
compileConstructorDescr ConstructorDef {..} =
  labelIden _constructorName
  <> compileBlocks _constructorDoc

compileTypeSignature :: [FunArg] -> Type -> LaTeX
compileTypeSignature args ret = mempty

-- | See https://tex.stackexchange.com/questions/257418/error-tightlist-converting-md-file-into-pdf-using-pandoc.
pandocTemplateExtra :: LaTeX
pandocTemplateExtra = raw [r|
\providecommand{\tightlist}{%
  \setlength{\itemsep}{0pt}\setlength{\parskip}{0pt}}
|]

extraPreface :: LaTeX
extraPreface = raw [r|
\hypersetup{colorlinks=true,urlcolor=RoyalBlue4,linkcolor=Salmon4,citecolor=Green4}
|]
  <> definecolor constructorColorStr "cb4b16"
  <> definecolor fieldColorStr "2aa198"

constructorColorStr :: Text
constructorColorStr = "constructorColor"

fieldColorStr :: Text
fieldColorStr = "fieldColor"

vertBarLeft :: Double -> LaTeX -> LaTeX
vertBarLeft f contents =
  minipageenv [FixArg $ fromString (showFloat f) <> textwidth]  mempty
  <>
  (hfill <> vline <> hfill)
  <>
  minipageenv [FixArg $ fromString (showFloat f') <> textwidth] contents
  where
  f' = 1 - f
