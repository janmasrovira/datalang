module DataLang.Parser.QQ where

import           Common
import qualified Data.Text                 as Text
import           DataLang.Parser
import           Language.Haskell.TH       (Exp, Q)
import           Language.Haskell.TH.Quote (QuasiQuoter (..))

moduleDefQ :: QuasiQuoter
moduleDefQ = QuasiQuoter
    { quotePat  = error "moduleDefQ: quasiquoter used in pattern context"
    , quoteType = error "moduleDefQ: quasiquoter used in type context"
    , quoteDec  = error "moduleDefQ: quasiquoter used in declaration context"
    , quoteExp  = dataLangProg
    }

dataLangProg :: String -> Q Exp
dataLangProg str = case parseModule (Text.pack str) of
  Left e -> fail ("Parser Error: " ++ text2String e)
    where
      text2String = Text.unpack
  Right r -> [| r |]
