module DataLang.Checker.Types where

import           Common
import           DataLang.Syntax.Extra

type FieldName = Text
type TypeName = Text
type ConstrName = Text
type FunctionName = Text
type ArgName = Text

type FieldsTable = HashMap FieldName Type
type SumTable = HashMap ConstrName Type

newtype CModuleDef = CModuleDef {
  _uncheck :: ModuleDef
  }

data GlobalCtx = GlobalCtx {
  _types             :: HashSet TypeName
  , _fieldDefs       :: HashSet FieldName
  , _constructorDefs :: HashSet ConstrName
  , _functions       :: HashSet FunctionName
  }
makeLenses ''GlobalCtx

newtype FunCtx = FunCtx {
  _args       :: HashSet ArgName
  }
makeLenses ''FunCtx

emptyGlobalCtx :: GlobalCtx
emptyGlobalCtx = GlobalCtx {
  _types = mempty
  , _fieldDefs = mempty
  , _constructorDefs = mempty
  , _functions = mempty
  }

emptyFunCtx :: FunCtx
emptyFunCtx = FunCtx {
  _args = mempty
  }
