module DataLang.Checker.Error where

import           Common
import           DataLang.Checker.Types
import           Prettyprinter
import           Prettyprinter.Render.Terminal

data Error =
  TypeNotDefined TypeName
  | DupName Text
  | DupArg ArgName
  deriving (Show)

prettyError :: Error -> Doc AnsiStyle
prettyError e = annotate (colorDull Red) $ case e of
    TypeNotDefined ty -> "Type" <+> prettyName ty <+> "is not defined."
    DupName iden      -> "The identifier" <+> prettyName iden <+> "is already defined."
    DupArg arg        -> "The argument" <+> prettyName arg <+> "is already used in the same function."
  <> "\n"

prettyName :: Text -> Doc AnsiStyle
prettyName = annotate bold . pretty

renderError :: Error -> Text
renderError = renderStrict . layoutPretty defaultLayoutOptions . prettyError

printErrorAnsi :: Error -> IO ()
printErrorAnsi = putDoc . prettyError
