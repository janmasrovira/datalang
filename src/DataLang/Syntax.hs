module DataLang.Syntax (
  module DataLang.Syntax
  , Pandoc
  ) where

import           Common
import           Data.Aeson
import           Data.HashMap.Strict    (HashMap (..), toList)
import           JsonSchema.Syntax
import           Lift.Aeson
import           Lift.HashMap
import           Lift.Pandoc
import           Text.Pandoc.Definition (Pandoc, Block)

-- | This types represents a leaf type, that is, which is not indexed by another
-- type.
data LeafType =
  -- | A user defined type.
  TyDataDef Text
  -- | The type of integers.
  | TyInteger
  -- | The type of strings.
  | TyString
  -- | The type of floats.
  | TyFloat
  -- | The type of booleans.
  | TyBool
 deriving (Show, Lift, Data)
makeLenses ''LeafType
makePrisms ''LeafType

data Type =
  Leaf LeafType
  | ListOf LeafType
 deriving (Show, Lift, Data)
makeLenses ''Type
makePrisms ''Type

data Annotation = Annotation {
  -- | A raw annotation.
  _rawAnnotation      :: Object
  -- | A schema annotation.
  , _schemaAnnotation :: Schema
  }
 deriving (Show, Lift, Data)
makeLenses ''Annotation

-- | A field definition.
data FieldDef =
  FieldDef {
   _fieldName          :: Text
   ,  _fieldType       :: Type
   ,  _fieldAnnotation :: Annotation
   ,  _fieldDoc        :: [Block]
   }
 deriving (Show, Lift, Data)
makeLenses ''FieldDef

data ConstructorArg =
  None
  | Of LeafType
  | Fields [FieldDef]
 deriving (Show, Lift, Data)
makeLenses ''ConstructorArg
makePrisms ''ConstructorArg

data ConstructorDef =
  ConstructorDef {
  _constructorName         :: Text
  , _constructorArg        :: ConstructorArg
  , _constructorAnnotation :: Annotation
  , _constructorDoc        :: [Block]
  }
 deriving (Show, Lift, Data)
makeLenses ''ConstructorDef

-- | The right hand side of a type definition.
data DataDefRhs =
  -- | A record definition.
  Record {
  -- | The fields.
     _fields :: [FieldDef]
     }
  -- | A sum type definition.
  | SumType {
  -- | The constructors.
      _constructors :: [ConstructorDef]
      }
  -- | A type synonym definition.
  | Synonym {
  -- | The synonym type
      _synonymType         :: LeafType
  -- | A json annotation
      , _synonymAnnotation :: Annotation
      }
 deriving (Show, Lift, Data)
makeLenses ''DataDefRhs
makePrisms ''DataDefRhs

data DataDef = DataDef {
  _name            :: Text
  , _rhs           :: DataDefRhs
  , _defAnnotation :: Annotation
  , _defDoc        :: [Block]
  }
 deriving (Show, Lift, Data)
makeLenses ''DataDef

-- | A function argument definition. The name is optional.
data FunArg =
  FunArg {
   _argName          :: Maybe Text
   ,  _argType       :: Type
   ,  _argAnnotation :: Annotation
   ,  _argDoc        :: [Block]
   }
 deriving (Show, Lift, Data)
makeLenses ''FunArg

data FunDef = FunDef {
  _funName            :: Text
  , _funArgs          :: [FunArg]
  , _funRet           :: Type
  , _funDefAnnotation :: Annotation
  , _funDefDoc        :: [Block]
  }
 deriving (Show, Lift, Data)
makeLenses ''FunDef

data ModuleDef = ModuleDef {
  _meta            :: HashMap Text Text
  , _documentation :: Pandoc
  , _typeDefs      :: [DataDef]
  , _functionDefs  :: [FunDef]
 }
 deriving (Show, Lift, Data)
makeLenses ''ModuleDef

instance Semigroup ModuleDef where
  a <> b = ModuleDef {
    _meta = _meta a <> _meta b
    , _documentation = _documentation a <> _documentation b
    , _typeDefs = _typeDefs a <> _typeDefs b
    , _functionDefs = _functionDefs a <> _functionDefs b
    }

instance Monoid ModuleDef where
  mempty = ModuleDef {
    _meta = mempty
    , _documentation = mempty
    , _typeDefs = mempty
    , _functionDefs = mempty
    }
