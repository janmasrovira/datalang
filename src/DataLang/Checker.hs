module DataLang.Checker (
  CModuleDef(_uncheck), checkModule
 , module DataLang.Checker.Error) where

import           Common
import           Control.Lens
import qualified Data.HashMap.Strict    as H
import qualified Data.HashSet           as HS
import           DataLang.Checker.Error
import           DataLang.Checker.Types
import           DataLang.Syntax.Extra

checkModule :: ModuleDef -> Either Error CModuleDef
checkModule m@ModuleDef {..} =
  CModuleDef m <$ runExcept
    (evalStateT (
    do
      mapM_ checkDataDef _typeDefs
      mapM_ checkFunDef _functionDefs
    ) emptyGlobalCtx)

findTypeDataDef :: (MonadReader GlobalCtx m, MonadError Error m) => TypeName -> m ()
findTypeDataDef ty = unlessM (isTypeDeclared ty)
                    (throwError (TypeNotDefined ty))

findLeafType :: (MonadReader GlobalCtx m, MonadError Error m) => LeafType -> m ()
findLeafType t = case t of
  TyDataDef name -> findTypeDataDef name
  _              -> return ()

findType :: (MonadReader GlobalCtx m, MonadError Error m) => Type -> m ()
findType t = case t of
  Leaf lt   -> findLeafType lt
  ListOf lt -> findLeafType lt

isFieldDeclared :: MonadReader GlobalCtx m => FieldName -> m Bool
isFieldDeclared field = asks (HS.member field . _fieldDefs)

isArgDeclared :: MonadReader FunCtx m => ArgName -> m Bool
isArgDeclared arg = asks (HS.member arg . _args)

isConstructorDeclared :: MonadReader GlobalCtx m => ConstrName -> m Bool
isConstructorDeclared constr = asks (HS.member constr . _constructorDefs)

isTypeDeclared :: MonadReader GlobalCtx m => TypeName -> m Bool
isTypeDeclared ty = asks (HS.member ty . _types)

isFunctionDeclared :: MonadReader GlobalCtx m => FunctionName -> m Bool
isFunctionDeclared fun = asks (HS.member fun . _functions)

checkFreeName :: (MonadReader GlobalCtx m, MonadError Error m) => Text -> m ()
checkFreeName name = whenM (
  orM [isFieldDeclared name
      , isConstructorDeclared name
      , isTypeDeclared name
      , isFunctionDeclared name])
  (throwError (DupName name))

checkFreeArg :: (MonadReader FunCtx m, MonadError Error m) => ArgName -> m ()
checkFreeArg name = whenM (isArgDeclared name) (throwError (DupArg name))

addTypeName :: (MonadState GlobalCtx m, MonadError Error m) => TypeName -> m ()
addTypeName ty = do
  freezes (checkFreeName ty)
  types %= HS.insert ty

checkFieldDef :: (MonadState GlobalCtx m, MonadError Error m) => FieldDef -> m ()
checkFieldDef FieldDef {..} = do
  freezes (checkFreeName _fieldName)
  fieldDefs %= HS.insert _fieldName
  freezes (findType _fieldType)

checkConstructorArg :: (MonadState GlobalCtx m, MonadError Error m) => ConstructorArg -> m ()
checkConstructorArg arg = case arg of
  None      -> return ()
  Of ty     -> freezes (findLeafType ty)
  Fields fs -> mapM_ checkFieldDef fs

checkConstructorDef :: (MonadState GlobalCtx m, MonadError Error m) => ConstructorDef -> m ()
checkConstructorDef ConstructorDef {..} = do
  freezes (checkFreeName _constructorName)
  constructorDefs %= HS.insert _constructorName
  checkConstructorArg _constructorArg

checkDataDef :: (MonadState GlobalCtx m, MonadError Error m) => DataDef -> m ()
checkDataDef DataDef{..} = case _rhs of
  Synonym{..} -> freezes (findLeafType _synonymType) >> addTypeName _name
  Record{..}  -> addTypeName _name >> mapM_ checkFieldDef _fields
  SumType{..} -> addTypeName _name >> mapM_ checkConstructorDef _constructors

checkFunArgName :: (MonadState FunCtx m, MonadError Error m) => Maybe ArgName -> m ()
checkFunArgName m = case m of
  Nothing -> return ()
  Just arg -> do
    freezes (checkFreeArg arg)
    args %= HS.insert arg

checkFunArg :: (MonadReader GlobalCtx m, MonadState FunCtx m, MonadError Error m) => FunArg -> m ()
checkFunArg FunArg {..} = do
  checkFunArgName _argName
  findType _argType

checkFunArgs :: (MonadReader GlobalCtx m, MonadError Error m) => [FunArg] -> m ()
checkFunArgs args = evalStateT (mapM_ checkFunArg args) emptyFunCtx

checkFunDef :: (MonadState GlobalCtx m, MonadError Error m) => FunDef -> m ()
checkFunDef FunDef{..} = do
  freezes (checkFreeName _funName)
  functions %= HS.insert _funName
  freezes (checkFunArgs _funArgs)
