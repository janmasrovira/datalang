module DataLang.Parser where

import           Common
import           Control.Applicative             (Alternative (many, some),
                                                  empty, (<|>))
import           Control.Applicative.Combinators
import           Data.Aeson
import qualified Data.HashMap.Strict             as HashMap
import qualified Data.Set                        as Set
import qualified Data.Text                       as Text
import qualified Data.Text.Encoding              as Text
import qualified Data.Text.IO                    as Text
import qualified Data.Yaml                       as Y
import           DataLang.Syntax.Extra
import           JsonSchema.Syntax               (SchemaNode (..))
import qualified Language.Haskell.TH.Syntax      as TH
import           Text.Megaparsec                 (MonadParsec, Parsec)
import qualified Text.Megaparsec                 as M
import           Text.Megaparsec.Char            (space1)
import qualified Text.Megaparsec.Char.Lexer      as L

space :: MonadParsec e Text m => m ()
space = L.space space1 line block
  where
    line = L.skipLineComment "#" <|> L.skipLineComment "//"
    block = L.skipBlockComment "(*" "*)"

lexeme :: MonadParsec e Text m => m a -> m a
lexeme = L.lexeme space

symbol :: MonadParsec e Text m => Text -> m ()
symbol = void . L.symbol space

symbol' :: MonadParsec e Text m => Text -> m ()
symbol' = void . L.symbol' space

listType :: MonadParsec e Text m => m Type
listType = do
  l <- leafType
  symbol' "list"
  return (ListOf l)

anyType :: MonadParsec e Text m => m Type
anyType = M.try listType <|> Leaf <$> leafType

leafType :: MonadParsec e Text m => m LeafType
leafType = do
  i <- identifier
  return $ case i of
    "integer" -> TyInteger
    "int"     -> TyInteger
    "bool"    -> TyBool
    "string"  -> TyString
    "float"   -> TyFloat
    _         -> TyDataDef i

kwType :: MonadParsec e Text m => m ()
kwType = symbol' "type"

kwFun :: MonadParsec e Text m => m ()
kwFun = symbol' "fun"

kwOf :: MonadParsec e Text m => m ()
kwOf = symbol' "of"

equal :: MonadParsec e Text m => m ()
equal = symbol "="

colon :: MonadParsec e Text m => m ()
colon = symbol ":"

rightArrow :: MonadParsec e Text m => m ()
rightArrow = symbol "→" <|> symbol "->"

pipe :: MonadParsec e Text m => m ()
pipe = symbol "|"

semicolon :: MonadParsec e Text m => m ()
semicolon = symbol ";"

parens :: MonadParsec e Text m => m a -> m a
parens = between (symbol "(") (symbol ")")

curly :: MonadParsec e Text m => m a -> m a
curly = between (symbol "{") (symbol "}")

constructorIden :: MonadParsec e Text m => m Text
constructorIden =
  lexeme $ do
  h <- M.satisfy isUpper
  Text.cons h <$> identifierTail

identifierTail :: MonadParsec e Text m => m Text
identifierTail = M.takeWhileP Nothing (isAlphaNum .||. (`elem`("_'" :: String)))

identifier :: MonadParsec e Text m => m Text
identifier =
  lexeme $ do
    h <- M.satisfy isLower
    Text.cons h <$> identifierTail

dataDefRhs :: MonadParsec e Text m => m DataDefRhs
dataDefRhs =
  synonymRhs
  <|> recordRhs
  <|> sumRhs

fieldDef :: MonadParsec e Text m => m FieldDef
fieldDef = M.label "field definition" $ do
  let _fieldDoc = mempty
  _fieldName <- identifier
  colon
  _fieldType <- anyType
  _fieldAnnotation <- jsonAnnotationOpt
  return FieldDef{..}

recordRhs' :: MonadParsec e Text m => m [FieldDef]
recordRhs' = curly (sepEndBy1 fieldDef semicolon)

synonymRhs :: MonadParsec e Text m => m DataDefRhs
synonymRhs = do
  _synonymAnnotation <- jsonAnnotationOpt
  _synonymType <- leafType
  return Synonym{..}

recordRhs :: MonadParsec e Text m => m DataDefRhs
recordRhs = Record <$> recordRhs'

pconstructorArg :: forall m e. MonadParsec e Text m => m ConstructorArg
pconstructorArg =
   M.try argOf
   <|> argFields
   <|> return None
  where
    argFields :: m ConstructorArg
    argFields = Fields <$> recordRhs'
    argOf :: m ConstructorArg
    argOf = kwOf >> Of <$> leafType

sumRhs :: MonadParsec e Text m => m DataDefRhs
sumRhs = do
  _constructors <- sepBy1 constructorDef pipe
  return SumType{..}

jsonAnnotationOpt :: MonadParsec e Text m => m Annotation
jsonAnnotationOpt = do
  obj <- fromMaybe mempty <$> optional jsonAnnotation
  return (Annotation obj emptySchema)

jsonAnnotation :: MonadParsec e Text m => m Object
jsonAnnotation = M.label ("JSON annotation: " ++ ljson ++ " ... " ++ rjson)
  $ lexeme $ do
  symbol ljson
  js <- M.someTill L.charLiteral (symbol rjson)
  case eitherDecodeStrict (Text.encodeUtf8 (pack js)) of
    Left e  -> M.failure Nothing (Set.singleton  (M.Label ('J':|"SON error: " ++ e)))
    Right r -> return (r :: Object)
  where
    ljson, rjson :: forall str. IsString str => str
    ljson = "@-"
    rjson = "-@"

dataDef :: MonadParsec e Text m => m DataDef
dataDef = M.label "type definition" $ do
  let _defDoc = mempty
  kwType
  _name <- identifier
  equal
  _defAnnotation <- jsonAnnotationOpt
  _rhs <- dataDefRhs
  return DataDef{..}

pfunArg :: forall e m. MonadParsec e Text m => m FunArg
pfunArg = M.label "function argument" (named <|> unnamed)
  where
  _argAnnotation = emptyAnnotation
  _argDoc = mempty
  named :: m FunArg
  named = parens $ do
    _argName <- Just <$> identifier
    colon
    _argType <- anyType
    return FunArg{..}

  unnamed :: m FunArg
  unnamed = do
    _argType <- anyType
    return FunArg{..}
      where
      _argName = Nothing

pfunArgs :: MonadParsec e Text m => m [FunArg]
pfunArgs = many (M.try (pfunArg <* rightArrow))

funDef :: MonadParsec e Text m => m FunDef
funDef = M.label "function definition" $ do
  let _funDefDoc = mempty
      _funDefAnnotation = emptyAnnotation
  kwFun
  _funName <- identifier
  colon
  _funArgs <- pfunArgs
  _funRet <- anyType
  return FunDef{..}

constructorDef :: MonadParsec e Text m => m ConstructorDef
constructorDef = do
  let _constructorDoc = mempty
  _constructorName <- constructorIden
  _constructorAnnotation <- jsonAnnotationOpt
  _constructorArg <- pconstructorArg
  return ConstructorDef{..}

pmodule :: MonadParsec e Text m => m ModuleDef
pmodule = do
  _meta <- pmetaOpt
  _typeDefs <- M.many dataDef
  _functionDefs <- M.many funDef
  let _documentation = mempty
  return ModuleDef{..}

pmetaOpt :: MonadParsec e Text m => m (HashMap Text Text)
pmetaOpt = fromMaybe mempty <$> optional pmeta

pmeta :: MonadParsec e Text m => m (HashMap Text Text)
pmeta =
  M.label ("Meta annotation (Yaml): " ++ ljson ++ " ... " ++ rjson)
  $ lexeme
  $ do
  symbol ljson
  js <- M.someTill L.charLiteral (symbol rjson)
  case Y.decodeEither' (Text.encodeUtf8 (pack js)) of
    Left e  -> M.failure Nothing (Set.singleton
      (M.Label ('Y':|"AML error: " ++ Y.prettyPrintParseException e)))
    Right r -> mkMeta r
  where
    ljson, rjson :: forall str. IsString str => str
    ljson = "---"
    rjson = "---"

mkMeta :: forall e m. MonadParsec e Text m => Object -> m (HashMap Text Text)
mkMeta obj = case metaFromObject obj of
  Left err -> M.failure Nothing (Set.singleton
      (M.Label ('Y':|"AML meta error: unexpected field type" ++ unpack err)))
  Right r -> return r

pmodule' :: Parsec Void Text ModuleDef
pmodule' = space >> pmodule <* M.eof

parseModule :: Text -> Either Text ModuleDef
parseModule input =
   case M.parse pmodule' "" input of
     Left e  -> throwError (Text.pack (M.errorBundlePretty e))
     Right r -> return r
