module DataLang.Syntax.Pretty where

import           Common
import           Control.Lens
import qualified Data.HashMap.Strict               as H
import           DataLang.Syntax.Extra             as S
import           DataLang.Syntax.Pretty.Annotation hiding (DataDef)
import qualified DataLang.Syntax.Pretty.Annotation as A
import           DataLang.Syntax.Pretty.BriefText
import           DataLang.Syntax.Pretty.Html
import           DataLang.Syntax.Pretty.Latex
import qualified DataLang.Syntax.Utils             as U
import           JsonSchema.Syntax                 (Schema)
import           JsonSchema.Syntax.Pretty
import           Prettyprinter
import           Prettyprinter.Render.Text
import           Text.Blaze.Html5                  (Html)

newtype Config = Config {
  _indent :: Int
  }

config :: Config
config = Config {
  _indent = 4
  }

hang' :: Doc Ann -> Doc Ann
hang' = hang (_indent config)

prettyAnnotation :: Kind -> Annotation -> Doc Ann
prettyAnnotation k Annotation{..} =
  nest (-99) $
  prettyRawAnnotation _rawAnnotation
  <> prettySchemaAnnotation _schemaAnnotation
  where
   prettyRawAnnotation :: Object -> Doc Ann
   prettyRawAnnotation obj
     | H.null obj = mempty
     | otherwise = annotate (Json KRaw k) $ pretty (prettyYaml obj)

   prettySchemaAnnotation :: Schema -> Doc Ann
   prettySchemaAnnotation s
     | isEmpty s = mempty
     | otherwise = annotate (Json KSchema k) $ pretty (prettyYaml s)

prettyIdenDef' :: Kind -> Text -> Text -> Annotation -> Doc Ann
prettyIdenDef' k nameQual nameDisplay annot = annDescr $ annotate (IdenDef k nameQual) $ pretty nameDisplay
  where annDescr = case getDescr annot of
          Nothing -> id
          Just d  -> annotate (Tooltip d)

prettyIdenDef :: Kind -> Text -> Annotation -> Doc Ann
prettyIdenDef k name = prettyIdenDef' k name name

prettyArgIdenDef :: Text -> Text -> Annotation -> Doc Ann
prettyArgIdenDef funName argName =
  prettyIdenDef' KArg (U.qualifyFunArg funName argName) argName

prettyFieldDef :: FieldDef -> Doc Ann
prettyFieldDef FieldDef{..} =
  annotate RhsItem
  $ hang' $ prettyIdenDef KField _fieldName _fieldAnnotation <+> ks colon
  <+> prettyType _fieldType
  <> prettyAnnotation KField _fieldAnnotation

prettyLeafType :: LeafType -> Doc Ann
prettyLeafType t = case t of
  TyInteger   -> primitive "int"
  TyString    -> primitive "string"
  TyFloat     -> primitive "float"
  TyBool      -> primitive "bool"
  TyDataDef t -> typeRef KType t

primitive :: Text -> Doc Ann
primitive prim = annotate (Tooltip "primitive type")
  . annotate (IdenRef KPrimitiveType prim) $ pretty prim

typeRef :: Kind -> Text -> Doc Ann
typeRef k ty = annotate (IdenRef k ty) (pretty ty)

prettyType :: Type -> Doc Ann
prettyType t = case t of
  Leaf l   -> prettyLeafType l
  ListOf l -> prettyLeafType l <+> primitive "list"

sepBy :: Foldable t => Doc Ann -> t (Doc Ann) -> Doc Ann
sepBy p = concatWith (\a b -> a <> p <> b)

prettyConstructorArg :: ConstructorArg -> Doc Ann
prettyConstructorArg arg = case arg of
  None          -> mempty
  Of lt         -> space <> kw "of" <+> prettyLeafType lt
  Fields fields -> space <> prettyFields fields

prettyFields :: [FieldDef] -> Doc Ann
prettyFields fields =
  braces $ annotate Rhs (line <> sepBy (line <> ks (annotate SepFieldSemi semi) <> space) (map prettyFieldDef fields) <> line)

prettyConstructorDef :: ConstructorDef -> Doc Ann
prettyConstructorDef ConstructorDef{..} =
  hang' (prettyIdenDef KConstructor _constructorName _constructorAnnotation
         <> prettyAnnotation KConstructor _constructorAnnotation
         <> prettyConstructorArg _constructorArg)

prettySynonymRhs :: LeafType -> Annotation -> Doc Ann
prettySynonymRhs ty ann = prettyAnnotation KType ann <> prettyLeafType ty

prettyDataDefRhs :: DataDefRhs -> Doc Ann
prettyDataDefRhs d = case d of
  SumType{..} -> line <> annotate Rhs
   (concatWith (\a b -> a <> line <> annotate RhsItem (ks pipe <+> b <> space))
   (map prettyConstructorDef _constructors))
  Record{..}  -> space <> prettyFields _fields
  Synonym{..} -> space <> prettySynonymRhs _synonymType _synonymAnnotation

kw :: Doc Ann -> Doc Ann
kw = annotate Keyword

ks :: Doc Ann -> Doc Ann
ks = annotate Keysym

prettyTypeNameDef :: Annotation -> Text -> Doc Ann
prettyTypeNameDef a name = prettyIdenDef KType name a

prettyFunNameDef :: Annotation -> Text -> Doc Ann
prettyFunNameDef a name = prettyIdenDef KFun name a

prettyArg :: Text -> FunArg -> Doc Ann
prettyArg funName FunArg {_argName = Just arg, ..} = parens
  (prettyArgIdenDef funName arg _argAnnotation <+> ks colon <+> prettyType _argType)
prettyArg _ FunArg {_argName = Nothing, ..} = prettyType _argType

rightArrow :: Doc Ann
rightArrow = ks "→"

prettyFunSignature :: Text -> [FunArg] -> Type -> Doc Ann
prettyFunSignature funName args ret = sepBy (" " <> rightArrow <> " ") (map (prettyArg funName) args <> [prettyType ret])

prettyDataDef :: DataDef -> Doc Ann
prettyDataDef DataDef{..} =
  annotate A.DataDef $
  hang' $ kw "type" <+> prettyTypeNameDef _defAnnotation _name <+> ks equals
  <> prettyAnnotation KType _defAnnotation
  <> prettyDataDefRhs _rhs

prettyFunDef :: FunDef -> Doc Ann
prettyFunDef S.FunDef{..} =
  annotate A.FunDef $
    hang' $ kw "fun" <+> prettyFunNameDef _funDefAnnotation _funName <+> ks colon
    <+> prettyAnnotation KFun _funDefAnnotation
    <> prettyFunSignature _funName _funArgs _funRet

prettyDataDefs :: [DataDef] -> [Doc Ann]
prettyDataDefs = map prettyDataDef

prettyFunctionDefs :: [FunDef] -> [Doc Ann]
prettyFunctionDefs = map prettyFunDef

prettyMeta :: HashMap Text Text -> Doc a
prettyMeta = mempty

prettyDocumentation :: Pandoc -> Doc a
prettyDocumentation = mempty

prettyModuleDef :: ModuleDef -> Doc Ann
prettyModuleDef ModuleDef {..} =
  prettyMeta _meta
  <> prettyDocumentation _documentation
  <> sepBy (line <> annotate TypeDefSep line)
     (prettyDataDefs _typeDefs <> prettyFunctionDefs _functionDefs)

prettyModuleDefText :: ModuleDef -> Text
prettyModuleDefText = renderStrict . layoutPretty defaultLayoutOptions . prettyModuleDef

prettyModuleDefBriefText :: ModuleDef -> Text
prettyModuleDefBriefText = renderBrief . layoutPretty defaultLayoutOptions . prettyModuleDef

prettyModuleDefHtml :: ModuleDef -> Text
prettyModuleDefHtml = renderHtml . layoutPretty defaultLayoutOptions . prettyModuleDef

prettyModuleDefLatex :: ModuleDef -> Text
prettyModuleDefLatex = renderLatex . layoutPretty defaultLayoutOptions . prettyModuleDef
