module DataLang.Syntax.Utils where

import           Common
import           Control.Lens
import qualified Data.HashMap.Strict   as H
import qualified Data.HashMap.Strict   as HashMap
import           DataLang.Syntax.Extra
import           Text.Pandoc
import qualified Data.Text as T

data DefinedIdentifierKind =
  KType
  | KRecField
  | KConstructorField
  | KConstructor
  | KFun
  | KArg


-- | Global identifiers are stored normally. Function arguments are stored with the form "funName.argName".
identifierKinds :: ModuleDef -> HashMap Text DefinedIdentifierKind
identifierKinds m = H.fromList (
  types ++ constructors ++ fields ++ constructorFields ++ functions ++ functionArgs)
  where
    types :: [(Text, DefinedIdentifierKind)]
    types = map (, KType) $
      m
      ^.. typeDefs
      . each
      . name
    constructors :: [(Text, DefinedIdentifierKind)]
    constructors = map (, KConstructor) $
      m
     ^.. typeDefs
     . each
     . rhs
     . _SumType
     . each
     . constructorName
    fields :: [(Text, DefinedIdentifierKind)]
    fields =
      map (, KRecField) $
      m
      ^.. typeDefs
      . each
      . rhs
      . _Record
      . each
      . fieldName
    functions :: [(Text, DefinedIdentifierKind)]
    functions = map (, KFun) $
      m
      ^.. functionDefs
      . each
      . funName
    constructorFields :: [(Text, DefinedIdentifierKind)]
    constructorFields = map (, KConstructorField) $
      m
     ^.. typeDefs
     . each
     . rhs
     . _SumType
     . each
     . constructorArg
     . _Fields
     . each
     . fieldName
    functionArgs :: [(Text, DefinedIdentifierKind)]
    functionArgs = map (, KArg) $
      [ qualifyFunArg (_funName f) arg | f <- m ^. functionDefs
          , arg <- f ^.. funArgs . each . argName . _Just
        ]

qualifyFunArg :: Text -> Text -> Text
qualifyFunArg funName argName = funName <> "." <> argName

-- | unqualify "a.b.c" = "c"
unqualify :: Text -> Text
unqualify = last . T.splitOn "."

lookupGlobalIden :: HashMap Text DefinedIdentifierKind -> Text -> Maybe DefinedIdentifierKind
lookupGlobalIden m iden = H.lookup iden m

lookupFunctionArg :: HashMap Text DefinedIdentifierKind -> Text -> Text -> Maybe DefinedIdentifierKind
lookupFunctionArg m fun arg = lookupGlobalIden m (qualifyFunArg fun arg)

addLinks :: ModuleDef -> ModuleDef
addLinks m = bottomUp addLink m
  where
  idens = identifierKinds m
  isIden = (`HashMap.member` idens)
  addLink :: Inline -> Inline
  addLink (Code attr t)
    | isIden t = Link mempty [Code attr t'] (selfLinkName t, t)
    where t' = unqualify t
  addLink k = k

tagIden :: IsString a => Text -> a
tagIden x = fromText $ "t:" <> x

selfLinkName :: IsString a => Text -> a
selfLinkName x = fromText $ "#" <> tagIden x
