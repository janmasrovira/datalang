module DataLang.Syntax.Extra (
  module DataLang.Syntax
  , module DataLang.Syntax.Extra) where

import           Common
import           Control.Lens
import           Control.Monad.Writer
import           Data.Aeson
import           Data.HashMap.Strict  (HashMap (..), toList)
import qualified Data.HashMap.Strict  as H
import qualified Data.HashMap.Strict  as HashMap
import           Data.Text            (Text, pack)
import           Data.Tuple.Extra
import           DataLang.Syntax
import           JsonSchema.Syntax
import           Text.Pandoc
import           Text.Pandoc.Generic
import qualified Pandoc.Extra as P

insertNode :: SchemaNode -> Schema -> Schema
insertNode n (Schema s) = Schema (H.insert (key n) n s)

schemaFromList :: [SchemaNode] -> Schema
schemaFromList = Schema . H.fromList . map (\n -> (key n, n))

objToRawNodesSchema :: Object -> Schema
objToRawNodesSchema = schemaFromList . objToRawNodes

objToRawNodes :: Object -> [SchemaNode]
objToRawNodes = map (uncurry RawNode) . toList

schemaToNodes :: Schema -> [SchemaNode]
schemaToNodes (Schema nodes) = H.elems nodes

annotationToNodes :: Annotation -> [SchemaNode]
annotationToNodes Annotation{..} =
  objToRawNodes _rawAnnotation
  ++ schemaToNodes _schemaAnnotation

emptySchema :: Schema
emptySchema = Schema mempty

emptyAnnotation :: Annotation
emptyAnnotation = Annotation mempty mempty

isEmpty :: Schema -> Bool
isEmpty (Schema l) = null l

getDescr :: Annotation -> Maybe Text
getDescr Annotation{..} = go1 _schemaAnnotation <|> go2 _rawAnnotation
  where
    descr = "description"
    go1 :: Schema -> Maybe Text
    go1 (Schema m) = case H.lookup descr m of
      Just (Description t) -> Just t
      _                    -> Nothing
    go2 :: Object -> Maybe Text
    go2 obj = case H.lookup descr obj of
      Just (String t) -> Just t
      _               -> Nothing

isTypeNamed :: Text -> DataDef -> Bool
isTypeNamed name d = name == _name d

isFunctionNamed :: Text -> FunDef -> Bool
isFunctionNamed name d = name == _funName d

atChild :: (Each s t a a, Applicative f) =>
     (a -> Bool) -> (a -> f a) -> s -> f t
atChild cond = each . filtered cond

isConstructorNamed :: Text -> ConstructorDef -> Bool
isConstructorNamed name d = name == _constructorName d

isFieldNamed :: Text -> FieldDef -> Bool
isFieldNamed name d = name == _fieldName d

isArgNamed :: Text -> FunArg -> Bool
isArgNamed name d = Just name == _argName d

atTypeDef :: (Each s t DataDef DataDef, Applicative f) =>
     Text -> (DataDef -> f DataDef) -> s -> f t
atTypeDef name = atChild (isTypeNamed name)

atField :: (Each s t FieldDef FieldDef, Applicative f) =>
     Text -> (FieldDef -> f FieldDef) -> s -> f t
atField name = atChild (isFieldNamed name)

atArg :: (Each s t FunArg FunArg, Applicative f) =>
     Text -> (FunArg -> f FunArg) -> s -> f t
atArg name = atChild (isArgNamed name)

atConstructor
  :: (Each s t ConstructorDef ConstructorDef, Applicative f) =>
     Text -> (ConstructorDef -> f ConstructorDef) -> s -> f t
atConstructor name = atChild (isConstructorNamed name)

pointType :: Applicative f => Text -> (DataDef -> f DataDef) -> ModuleDef -> f ModuleDef
pointType tyName =
  typeDefs
  . each
  . filtered (isTypeNamed tyName)

pointFunction :: Applicative f => Text -> (FunDef -> f FunDef) -> ModuleDef -> f ModuleDef
pointFunction funName =
  functionDefs
  . each
  . filtered (isFunctionNamed funName)

pointConstructor :: Applicative f => Text -> Text -> (ConstructorDef -> f ConstructorDef) -> ModuleDef -> f ModuleDef
pointConstructor tyName constrName =
   pointType tyName
  . rhs
  . _SumType
  . atConstructor constrName

pointField :: Applicative f => Text -> Text -> (FieldDef -> f FieldDef) -> ModuleDef -> f ModuleDef
pointField tyName fieldName =
  pointType tyName
  . rhs
  . _Record
  . atField fieldName

pointArg :: Applicative f => Text -> Text -> (FunArg -> f FunArg) -> ModuleDef -> f ModuleDef
pointArg funName argName =
  pointFunction funName
  . funArgs
  . atArg argName

pointConstructorField :: Applicative f => Text -> Text -> Text -> (FieldDef -> f FieldDef) -> ModuleDef -> f ModuleDef
pointConstructorField tyName constrName fieldName =
  pointConstructor tyName  constrName
  . constructorArg
  . _Fields
  . atField fieldName

annotateField :: Text -> Text -> Schema -> ModuleDef -> ModuleDef
annotateField tyName fieldName f mod =
  mod
  & pointField tyName fieldName
  . fieldAnnotation
  . schemaAnnotation
  %~ (<>f)

annotateConstructorField :: Text -> Text -> Text -> Schema -> ModuleDef -> ModuleDef
annotateConstructorField tyName constrName fieldName f mod =
  mod
  & pointConstructorField tyName constrName fieldName
  . fieldAnnotation
  . schemaAnnotation
  %~ (<>f)

annotateType :: Text -> Schema -> ModuleDef -> ModuleDef
annotateType tyName f mod =
  mod
  & pointType tyName
  . defAnnotation
  . schemaAnnotation
  %~ (<>f)

documentTypeDef :: Text -> [Block] -> ModuleDef -> ModuleDef
documentTypeDef tyName doc mod
 | not (typeDefExists tyName mod) = errorT $ "type definition " <> tyName <> " does not exist"
 | otherwise = documentTypeDefUnsafe tyName doc mod

documentTypeDefUnsafe :: Text -> [Block] -> ModuleDef -> ModuleDef
documentTypeDefUnsafe tyName doc mod =
  mod
  & pointType tyName
  . defDoc
  .~ doc

documentConstructor :: Text -> Text -> [Block] -> ModuleDef -> ModuleDef
documentConstructor tyName constrName doc mod
 | not (constructorExists tyName constrName mod) =
   errorT $ "constructor " <> tyName <> "." <> constrName <> " does not exist"
 | otherwise =
  mod
  & pointConstructor tyName constrName
  . constructorDoc
  .~ doc

documentFunctionUnsafe :: Text -> [Block] -> ModuleDef -> ModuleDef
documentFunctionUnsafe funName doc mod =
  mod
  & pointFunction funName
  . funDefDoc
  .~ doc

documentFunction :: Text -> [Block] -> ModuleDef -> ModuleDef
documentFunction funName doc mod
 | not (functionExists funName mod) =
   errorT $ "function " <> funName <> " does not exist"
 | otherwise = documentFunctionUnsafe funName doc mod

documentArgUnsafe :: Text -> Text -> [Block] -> ModuleDef -> ModuleDef
documentArgUnsafe funName argName doc mod =
  mod
  & pointArg funName argName
  . argDoc
  .~ doc

documentArg :: Text -> Text -> [Block] -> ModuleDef -> ModuleDef
documentArg funName argName doc mod
 | not (fieldExists funName argName mod) =
   errorT $ "argument " <> funName <> "." <> argName <> " does not exist"
 | otherwise = documentArgUnsafe funName argName doc  mod

documentFieldUnsafe :: Text -> Text -> [Block] -> ModuleDef -> ModuleDef
documentFieldUnsafe tyName fieldName doc mod =
  mod
  & pointField tyName fieldName
  . fieldDoc
  .~ doc

documentField :: Text -> Text -> [Block] -> ModuleDef -> ModuleDef
documentField tyName fieldName doc mod
 | not (fieldExists tyName fieldName mod) =
   errorT $ "field " <> tyName <> "." <> fieldName <> " does not exist"
 | otherwise = documentFieldUnsafe tyName fieldName doc  mod

documentConstructorField :: Text -> Text -> Text -> [Block] -> ModuleDef -> ModuleDef
documentConstructorField tyName constrName fieldName doc mod
 | not (constructorFieldExists tyName constrName fieldName mod) =
   errorT $ "field " <> tyName <> "." <> constrName <> "." <> fieldName <> " does not exist"
 | otherwise =
  mod
  & pointConstructorField tyName constrName fieldName
  . fieldDoc
  .~ doc

functionExists :: Text -> ModuleDef -> Bool
functionExists = has . pointFunction

typeDefExists :: Text -> ModuleDef -> Bool
typeDefExists = has . pointType

constructorExists :: Text -> Text -> ModuleDef -> Bool
constructorExists tyName = has . pointConstructor tyName

fieldExists :: Text -> Text -> ModuleDef -> Bool
fieldExists tyName = has . pointField tyName

argExists :: Text -> Text -> ModuleDef -> Bool
argExists funName = has . pointArg funName

constructorFieldExists :: Text -> Text -> Text -> ModuleDef -> Bool
constructorFieldExists tyName constrName = has . pointConstructorField tyName constrName

metaFromObject :: forall m. MonadError Text m => Object -> m (HashMap Text Text)
metaFromObject = fmap H.fromList . mapM (secondM aux) . H.toList
  where
  aux :: Value -> m Text
  aux v = case v of
   String t   -> return t
   Number s   -> return $ pack (show s)
   Bool False -> return "false"
   Bool True  -> return "true"
   _          -> throwError ("unexpected field type" <> showT v)

queryImagesRelPaths :: ModuleDef -> [FilePath]
queryImagesRelPaths = filter isRelative . queryImagesPaths

queryImagesPaths :: ModuleDef -> [FilePath]
queryImagesPaths = queryWith P.getImagePath
