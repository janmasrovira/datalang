module DataLang.Syntax.Pretty.BriefText (renderBrief) where

import           Common
import qualified Data.Text                               as T
import qualified Data.Text.Lazy                          as LT
import qualified Data.Text.Lazy.Builder                  as TLB
import           DataLang.Syntax.Pretty.Annotation
import           Prettyprinter
import           Prettyprinter.Render.Util.SimpleDocTree

renderTree :: SimpleDocTree Ann -> TLB.Builder
renderTree sds = case sds of
    STEmpty           -> mempty
    STChar c          -> TLB.singleton c
    STText _ t        -> TLB.fromText t
    STLine i          -> "\n" <> TLB.fromText (textSpaces i)
    STAnn ann content -> case ann of
      Json{} -> mempty
      _      -> renderTree content
    STConcat contents -> foldMap renderTree contents

renderBrief :: SimpleDocStream Ann -> Text
renderBrief =  LT.toStrict . TLB.toLazyText . renderTree . treeForm
