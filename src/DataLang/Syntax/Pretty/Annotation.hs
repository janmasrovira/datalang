module DataLang.Syntax.Pretty.Annotation where

import Common

data Kind =
   KField
  | KType
  | KFun
  | KArg
  | KConstructor
  | KPrimitiveType

data JsonKind =
  KRaw
  | KSchema

data Ann =
  SepFieldSemi
  | Json JsonKind Kind
  | DataDef
  | FunDef
  | Rhs
  | RhsItem
  | Tooltip Text
  | Keyword
  | Keysym
  | IdenRef Kind Text
  | IdenDef Kind Text
  | TypeDefSep
