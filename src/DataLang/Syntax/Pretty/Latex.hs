module DataLang.Syntax.Pretty.Latex (renderLatex, renderTree) where

import           Common
import qualified Data.Text                               as T
import           DataLang.Syntax.Pretty.Annotation
import           DataLang.Syntax.Utils
import           Latex.Extra
import           Prettyprinter
import           Prettyprinter.Render.Util.SimpleDocTree

renderTree :: SimpleDocTree Ann -> LaTeX
renderTree = go

go :: SimpleDocTree Ann -> LaTeX
go sdt = case sdt of
    STEmpty           -> mempty
    STChar c          -> fromString [c]
    STText _ t        -> raw t
    STLine i          -> fromText ("\n" <> textSpaces i)
    STAnn ann content -> putTag ann (go content)
    STConcat contents -> mconcatMap go contents

putTag :: Ann -> LaTeX -> LaTeX
putTag sh x = case sh of
  Keyword        -> textbf x
  Keysym         -> textbf x
  Json{}         -> mempty
  IdenRef _ iden -> linkIden iden x
  IdenDef _ iden -> labelIden x iden
  TypeDefSep     -> x
  Rhs            -> x
  RhsItem        -> x
  DataDef        -> x
  FunDef         -> x
  SepFieldSemi   -> x
  Tooltip tt     -> x

renderLatex :: SimpleDocStream Ann -> Text
renderLatex = render . renderTree . treeForm

labelIden :: LaTeX -> Text -> LaTeX
labelIden capt = hypertarget capt . tagIden

linkIden :: Text -> LaTeX -> LaTeX
linkIden iden x = hyperlink x (tagIden iden)
