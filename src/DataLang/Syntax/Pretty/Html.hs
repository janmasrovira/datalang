module DataLang.Syntax.Pretty.Html (renderHtml) where

import           Common
import qualified Data.Text                               as T
import qualified Data.Text.Lazy                          as LT
import           DataLang.Syntax.Pretty.Annotation
import           Prettyprinter
import           Prettyprinter.Render.Util.SimpleDocTree
import qualified Text.Blaze.Html.Renderer.Text           as Html
import           Text.Blaze.Html5                        as Html
import qualified Text.Blaze.Html5.Attributes             as Attr

type Color = Text
type FontSize = Text

kwColor :: Color
kwColor = "#c43063"

symColor :: Color
symColor = "#df443f"

fieldColor :: Color
fieldColor = "#388057"

typeRefColor :: Color
typeRefColor = "#de7185"

typeNameColor :: Color
typeNameColor = "#e98176"

annotBorderColor :: Kind -> Color
annotBorderColor k = case k of
-- "#35556a"
  KField       -> fieldColor
  KConstructor -> constructorColor
  KType        -> typeNameColor
  _            -> typeNameColor

constructorColor :: Color
constructorColor = "#2f5d62"

defFontSize :: AttributeValue
defFontSize = "14pt"

renderTree :: SimpleDocTree Ann -> Html
renderTree sdt =
    docType
    <> style css
    <> (Html.body ! sty1 $
      pre ! sty2 $
      go sdt)
  where
    sty1 = Attr.style ("background: #fdf6e3; overflow:auto; width:auto; font-size:"
                       <> defFontSize <> ";padding: 3em 15% 5em 15%")
    sty2 = Attr.style "margin: 0; line-height: 125%"

go :: SimpleDocTree Ann -> Html
go sdt = case sdt of
    STEmpty           -> mempty
    STChar c          -> toHtml c
    STText _ t        -> toHtml t
    STLine i          -> "\n" <> toHtml (textSpaces i)
    STAnn ann content -> putTag ann (go content)
    STConcat contents -> mconcatMap go contents

color :: Color -> Attribute
color col = Attr.style (fromText $ "color:" <> col)

fontSize :: FontSize -> Attribute
fontSize col = Attr.style (fromText $ "font-size:" <> col)

tooltip :: Attribute
tooltip = Attr.class_ "tooltip"

tooltiptext :: Attribute
tooltiptext = Attr.class_ "tooltiptext"

putTag :: Ann -> Html -> Html
putTag sh x = case sh of
  Keyword               -> strong x ! color kwColor
  Keysym                -> Html.span x ! color symColor
  Json{}             -> mempty
  IdenRef _ lab           -> a ! Attr.href (fromText ("#" <> lab))
                             ! color typeRefColor
                             $ x
  IdenDef _ lab           -> u $ Html.a ! Attr.name (fromText lab)
                             ! color typeNameColor
                             $ x
  TypeDefSep            -> br <> hr <> x
  Rhs                   -> x
  RhsItem               -> x
  DataDef               -> x
  FunDef                -> x
  SepFieldSemi          -> x
  Tooltip tt            -> Html.div ! tooltip
                           $ x <> (Html.span ! tooltiptext) (toHtml tt)

jsonDetails :: Kind -> Html -> Html
jsonDetails k x = Html.div ! Attr.style "margin:1em 3em 1em 3em" $ details x ! sty
  where
    sty = Attr.style
     (fromText $ "overflow:auto;width:auto;border:solid "
      <> annotBorderColor k <> ";border-width:.1em .1em .1em .3em; padding:.2em .6em")

renderHtml :: SimpleDocStream Ann -> Text
renderHtml =  LT.toStrict . Html.renderHtml . renderTree . treeForm

css :: Html
css = toHtml $ T.strip [r|
.tooltip {
  position: relative;
  display: inline-block;
  border-bottom: 1px dotted black;
}

.tooltip .tooltiptext {
  visibility: hidden;
  width: auto;
  background-color: #ffffaa;
  border: .15em solid #ffad33;
  color: #17170f;
  text-align: center;
  border-radius: .6em;
  padding: .2em .8em;

  /* Position the tooltip */
  position: absolute;
  z-index: 1;
}

.tooltip:hover .tooltiptext {
  visibility: visible;
}
|]
