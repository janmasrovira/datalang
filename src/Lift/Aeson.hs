{-# LANGUAGE CPP                  #-}
module Lift.Aeson where

import           Data.Aeson
import qualified Data.HashMap.Strict        as H
import qualified Data.Text                  as Text
import           Data.Tuple.Extra
import           Language.Haskell.TH.Syntax (Lift)
import qualified Language.Haskell.TH.Syntax as TH

-- | Instance copied from <https://hackage.haskell.org/package/aeson-1.5.6.0/docs/src/Data.Aeson.Types.Internal.html#line-440>.
instance Lift Object where
   lift o = [| (H.fromList . map (first pack) $ o') |]
      where o' = map (first Text.unpack) . H.toList $ o
#if MIN_VERSION_template_haskell(2,17,0)
   liftTyped = TH.unsafeCodeCoerce . TH.lift
#elif MIN_VERSION_template_haskell(2,16,0)
   liftTyped = TH.unsafeTExpCoerce . TH.lift
#endif
