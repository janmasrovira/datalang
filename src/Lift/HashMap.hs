{-# LANGUAGE CPP #-}
module Lift.HashMap where

import qualified Data.HashMap.Strict        as H
import           Data.Text                  (Text)
import qualified Data.Text                  as Text
import           Data.Tuple.Extra
import           Language.Haskell.TH.Syntax (Lift)
import qualified Language.Haskell.TH.Syntax as TH

instance Lift (H.HashMap Text Text) where
   lift o = [| (H.fromList . map (first pack) $ o') |]
      where o' = map (first Text.unpack) . H.toList $ o
#if MIN_VERSION_template_haskell(2,17,0)
   liftTyped = TH.unsafeCodeCoerce . TH.lift
#elif MIN_VERSION_template_haskell(2,16,0)
   liftTyped = TH.unsafeTExpCoerce . TH.lift
#endif
