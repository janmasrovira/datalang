{-# LANGUAGE StandaloneDeriving #-}
module Lift.Pandoc where

import           Common
import           Data.Map.Internal
import           Text.Pandoc.Definition

deriving instance Lift ColSpan
deriving instance Lift RowSpan
deriving instance Lift Cell
deriving instance Lift RowHeadColumns
deriving instance Lift TableFoot
deriving instance Lift TableBody
deriving instance Lift Row
deriving instance Lift TableHead
deriving instance Lift ColWidth
deriving instance Lift Alignment
deriving instance Lift Caption
deriving instance Lift ListNumberDelim
deriving instance Lift ListNumberStyle
deriving instance Lift CitationMode
deriving instance Lift Format
deriving instance Lift MathType
deriving instance Lift Citation
deriving instance Lift QuoteType
deriving instance Lift Inline
deriving instance Lift Block
deriving instance Lift MetaValue
deriving instance Lift (Map Text MetaValue)
deriving instance Lift Meta
deriving instance Lift Pandoc
