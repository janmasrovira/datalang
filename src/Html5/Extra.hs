module Html5.Extra
  (module Html5.Extra
  , module Html
  )
  where

import           Common
import           Text.Blaze.Html5            as Html hiding (map)
import qualified Text.Blaze.Html5.Attributes as Attr
import qualified Text.Blaze.Internal         as Html

math :: Html -> Html
math = Html.Parent "math" "<math" "</math>"

mathBlock :: Html -> Html
mathBlock = math ! customAttribute "display" "block"

mathJaxCdn :: Html
mathJaxCdn = script1 <> script2
  where
  script1 = script
      ! Attr.src src1
      $ mempty
  script2 = script
      ! Attr.type_ "text/javascript"
      ! Attr.id  "MathJax-script"
      ! Attr.src src2
      $ mempty
  src1 = "https://polyfill.io/v3/polyfill.min.js?features=es6"
  src2 = "https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-chtml.js"

-- | This is useful fore debugging only. Note that it only works on a server
-- protocol, opening the file from the local system won't work. For that, one
-- can use @python3 -m http.server@.
livejs :: Html
livejs = script
  ! Attr.type_ "text/javascript"
  ! Attr.src "https://livejs.com/live.js"
  $ mempty

linuwialCss :: Html
linuwialCss =
   link ! Attr.href "assets/linuwial.css"
        ! Attr.rel "stylesheet"
        ! Attr.type_ "text/css"
        ! Attr.title "Linuwial"

sourceCss :: Html
sourceCss = link ! Attr.href "assets/source.css"
        ! Attr.rel "stylesheet"
        ! Attr.type_ "text/css"

highlightJs :: Html
highlightJs = script ! Attr.src "assets/highlight.js"
        ! Attr.type_ "text/javascript"
        $ mempty

metaUtf8 :: Html
metaUtf8 = meta ! Attr.charset "UTF-8"

mono :: Html -> Html
mono = Html.span ! Attr.style "font-family:'Lucida Console', monospace"

href :: Text -> Html -> Html
href url = a ! Attr.href (fromText url)

hx :: Int -> Html -> Html
hx i = case i of
  0 -> h1
  1 -> h1
  2 -> h2
  3 -> h3
  4 -> h4
  5 -> h5
  6 -> h6
  _ -> error "wrong depth"
