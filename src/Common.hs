module Common
   (module Common
   , module Control.Lens.TH
   , module Control.Monad.Except
   , module Control.Monad.Extra
   , module Control.Monad.Identity
   , module Control.Monad.Reader
   , module Control.Monad.State
   , module Data.Char
   , module Data.Either.Extra
   , module Data.Function
   , module Data.List.Extra
   , module Data.Maybe
   , module Data.String
   , module Data.Text.Encoding
   , module Data.Tree
   , module Data.Tuple.Extra
   , module Data.Void
   , module System.Directory
   , module System.FilePath
   , module Text.RawString.QQ
   , Data
   , Text
   , pack
   , unpack
   , strip
   , HashMap
   , Pretty
   , pretty
   , ByteString
   , Vector
   , Lift
   , NonEmpty(..)
   , Set
   , HashSet
   , Object
   , ToJSON
   , IsString(..)
   , Alternative(..)
   )
where

import           Control.Applicative
import           Control.Lens.TH
import           Control.Monad.Except
import           Control.Monad.Extra
import           Control.Monad.Identity
import           Control.Monad.Reader
import           Control.Monad.State
import           Control.Monad.Writer
import           Data.Aeson                 (Object, ToJSON)
import           Data.Aeson.Encode.Pretty   as P
import           Data.ByteString.Lazy       (ByteString)
import qualified Data.ByteString.Lazy       as B
import           Data.Char
import           Data.Data
import           Data.Either.Extra
import           Data.Function
import           Data.HashMap.Strict        (HashMap)
import           Data.HashSet               (HashSet)
import           Data.List                  (foldl')
import           Data.List.Extra
import           Data.List.NonEmpty         (NonEmpty (..))
import           Data.Maybe
import           Data.Set                   (Set)
import           Data.String
import           Data.Text                  (Text, pack, strip, unpack)
import qualified Data.Text                  as Text
import           Data.Text.Encoding
import qualified Data.Text.IO               as Text
import           Data.Tree
import           Data.Tuple.Extra
import           Data.Vector                (Vector)
import           Data.Void
import           Data.Yaml                  as Yaml
import           Language.Haskell.TH.Syntax (Lift)
import           Prettyprinter              (Pretty, pretty)
import           System.Directory
import           System.FilePath
import           Text.RawString.QQ

prettyJson :: ToJSON a => a -> Text
prettyJson = decodeUtf8 . B.toStrict . encodePretty

prettyYaml :: ToJSON a => a -> Text
prettyYaml = decodeUtf8 . Yaml.encode

(.||.) :: (a -> Bool) -> (a -> Bool) -> a -> Bool
(f .||. g) x = f x || g x

tell1 :: (Applicative f, MonadWriter (f e) m) => e -> m ()
tell1 = tell . pure

pipeApp :: [a -> a] -> a -> a
pipeApp ts a = foldl' (flip ($)) a ts

-- | Lowers the first letter of a 'Text'.
uncapitalize :: Text -> Text
uncapitalize (Text.uncons -> Nothing)      = ""
uncapitalize (Text.uncons -> Just (l, ls)) = Text.cons (toLower l) ls

fromRightIO :: Either Text a -> IO a
fromRightIO e = case e of
  Right r  -> return r
  Left err -> fail (show err)

textSpaces :: Int -> Text
textSpaces n = Text.replicate n (Text.singleton ' ')

errorT :: Text -> a
errorT = error . unpack

showT :: Show s => s -> Text
showT = pack . show

fromText :: IsString s => Text -> s
fromText = fromString . unpack

freezes :: MonadState a m => ReaderT a m b -> m b
freezes x = get >>= runReaderT x
