module Example where

import           Common
import           Data.Aeson
import           Data.Aeson.QQ.Simple
import qualified Data.Text.IO                 as Text
import           DataLang.Compiler.JsonSchema
import qualified DataLang.Compiler.Ocaml      as O
import           DataLang.Parser.QQ
import           DataLang.Syntax.Extra
import           JsonSchema.Builder
import           JsonSchema.Syntax
import qualified Ocaml.Syntax.Pretty          as O
import           Text.Pandoc
import           Text.Pandoc.Options
import           Text.Pandoc.Readers

prog1 :: ModuleDef
prog1 = [moduleDefQ|
  type timestamp =
   @- {"minimum": 946684822,
       "maximum":253402300826} -@
     int
  type myty =
  C1
    { paramForC1 : timestamp
     }
    | C2 {
     paramForC2 : string
     ; otherParam : int
    }
    | C3
|]
prog2 :: ModuleDef
prog2 = [moduleDefQ|
  fun add : (a : int) -> (b : str) -> int
  fun subtr : (a : int) -> (b : str) → int
|]

t :: IO ()
t = do
  Text.putStrLn (compileModuleText prog1)

f :: IO ()
f = do
  -- Text.putStrLn (O.prettyModuleDefText (O.compileModule prog1))
  Text.putStrLn (O.prettyModuleDefText (O.compileModule prog1))


rp :: IO ()
rp = do
  t <- Text.readFile "test.tex"
  Right p <- runIO (readLaTeX def t)
  print p
  putStrLn ""
  Right o <- runIO (writeLaTeX def p)
  Text.putStrLn o
