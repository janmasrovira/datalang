module DataLangUtils (compileAll, exportAll, readOrg) where

import           Common
import qualified Coq.Syntax                    as C
import qualified Coq.Syntax.Pretty             as C
import qualified Data.Text.IO                  as Text
import qualified DataLang.Compiler.Coq         as C
import qualified DataLang.Compiler.Html        as Html
import           DataLang.Compiler.Html.Source
import           DataLang.Compiler.JsonSchema
import qualified DataLang.Compiler.Latex       as Latex
import qualified DataLang.Compiler.Ocaml       as O
import           DataLang.OrgParser
import           DataLang.Syntax.Extra
import           DataLang.Syntax.Pretty
import           JsonSchema.Syntax
import           JsonSchema.Syntax.Pretty
import qualified Ocaml.Syntax                  as O
import qualified Ocaml.Syntax.Pretty           as O
import           Pandoc.Debug
import           Paths
import           System.Directory
import           System.FilePath
import DataLang.Checker

compileAll :: ModuleDef -> IO (Schema, O.ModuleDef, C.ModuleDef, ModuleDef)
compileAll mod = do
  schema <- (fromRightIO . compileModule) mod
  return (schema, O.compileModule mod, C.compileModule mod, mod)

writeFileAndSay :: FilePath -> Text -> IO ()
writeFileAndSay f txt = do
  Text.writeFile f txt
  putStrLn ("wrote " <> f)

exportAll :: FilePath -- ^ Target build directory
   -> String -- ^ Target name
   -> FilePath -- ^ Org file
   -> IO ()
exportAll buildDir baseName org = do
  mod <- readOrg org >>= either (\e -> printErrorAnsi e >> ioError (userError "checking failed"))
    (return . _uncheck) . checkModule
  let imgPaths = queryImagesRelPaths mod
  pandocShow <- readPandocShow org
  (js, oc, coq, dl) <- compileAll mod
  createDirectoryIfMissing True buildDir
  copyAssetFiles buildDir
  copyImageFiles org buildDir imgPaths
  withCurrentDirectory buildDir $ do
   writeFileAndSay (f "-doc.html") (Html.compileModuleHtmlText baseName dl)
   writeFileAndSay (f "-doc.tex") (Latex.compileModuleText dl)
   writeFileAndSay (f ".html") (prettyModuleDefHtml dl)
   writeFileAndSay (f "-src.html") (prettySourceText baseName dl)
   writeFileAndSay (f "-src.tex") (prettyModuleDefLatex dl)
   writeFileAndSay (f ".dl") (prettyModuleDefText dl)
   writeFileAndSay (f "-brief.dl") (prettyModuleDefBriefText dl)
   writeFileAndSay (f ".json") (prettyJson js)
   writeFileAndSay (f ".ml") (O.prettyModuleDefText oc)
   writeFileAndSay (f ".v") (C.prettyModuleDefText coq)
   writeFileAndSay (f "-pandoc.txt") pandocShow
  where f = (baseName <>)

-- | Copies images to the build directory.
copyImageFiles ::
  -- | Org path.
  FilePath
  -- | Build (target) directory.
  -> FilePath
  -- | Each element is a relative (w.r.t. the org file) path to an image file.
  -> [FilePath]
  -> IO ()
copyImageFiles orgFile buildDir = mapM_ copyImage
  where
  orgDir = takeDirectory orgFile
  copyImage :: FilePath -> IO ()
  copyImage img = cpSafe (orgDir </> img) (buildDir </> img)
  cpSafe :: FilePath -> FilePath -> IO ()
  cpSafe source target = whenM (doesFileExist source) $ do
    createDirectoryIfMissing True targetDir
    putStrLn $ "exporting image to " <> target
    copyFile source target
    where targetDir = takeDirectory target

copyAssetFiles :: FilePath -> IO ()
copyAssetFiles targetBuildDir = do
  createDirectoryIfMissing True targetAssetsDir
  let x = ".latexmkrc" in copyFile (fromAssetsDir </> x) (targetBuildDir </> x)
  mapM_ cpFile files
  where
  fromAssetsDir = $(assetsDir)
  toAssetsDir = targetBuildDir </> "assets"
  cpFile (fromDir, name, toDir) = copyFile (fromDir </> name) (toDir </> name)
  targetAssetsDir = targetBuildDir </> "assets"
  files = assetFiles ++ otherFiles
  assetFiles = [ (fromAssetsDir, name, toAssetsDir)
               | name <- ["extra.css"
            , "highlight.js"
            , "linuwial.css"
            , "source.css"
            , "synopsis.png"]]
  otherFiles = [(fromAssetsDir, ".latexmkrc", targetBuildDir)]

readOrg :: FilePath -> IO ModuleDef
readOrg orgFile = Text.readFile orgFile >>= fromRightIO . parseOrgDataLang
