module Paths where

import Common
import TH.RelativePaths
import Language.Haskell.TH.Syntax as TH

assetsDir :: Q Exp
assetsDir = TH.lift =<< pathRelativeToCabalPackage "assets"
