module Main where

import           DataLang.Parser.QQ
import           DataLang.Parser
import           DataLang.Syntax
import           Test.Tasty
import           Test.Tasty.HUnit
import           Common

main :: IO ()
main = defaultMain tests

tests :: TestTree
tests = testGroup "Tests" [unitTests]

assertTrue :: HasCallStack => Bool -> Assertion
assertTrue = assertBool ""

assertRight :: HasCallStack => Either l r -> Assertion
assertRight = assertBool "" . isRight

parserTestCase :: TestName -> Text -> TestTree
parserTestCase descr = testCase descr . assertRight . parseModule

unitTests :: TestTree
unitTests = testGroup "Unit tests"
  [ parserTestCase "basic type synonym"
    [r|
      type timestamp = int
      |]
      ,
     parserTestCase "basic constructor"
    [r|
      type sum_type = A | B
      |]
      ,
     parserTestCase "Rectangle constructor"
    [r|
      type rectangle = {height: int; width: int}
      |]
      ,
     parserTestCase "Circle constructor"
    [r|
      type circle = {radius: int}
      |]
      ,
     parserTestCase "Rectangle annotated"
    [r|
      type rectangle = {height: int @- {"Field": "This is annotation"} -@; width: int}
      |]
  ]
